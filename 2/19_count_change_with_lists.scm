;; Number [List-of Number] -> Number
(define (cc amount coin-values)
  (define no-more? null?)
  (define first-denomination car)
  (define except-first-denomination cdr)
  (cond ((= amount 0) 1)
	((or (< amount 0) (no-more? coin-values)) 0)
	(else
	 (+ (cc amount
		(except-first-denomination coin-values))
	    (cc (- amount
		   (first-denomination coin-values))
		coin-values)))))

;; The order of the list coin-values doesn't affect the
;; answer produced by cc, because no matter which denomination
;; is used first, the number of ways in which we can make a change
;; of a given amount remains the same. It equals the numbers of
;; ways in which it can be changed without using the first denomination
;; plus the number of ways in which change can be made after
;; using a coin of the first kind.

(define us-coins (list 50 25 10 5 1))

(newline)
(display (cc 159 us-coins))
(newline)
(display (cc 159 (reverse us-coins)))
