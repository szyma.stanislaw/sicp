(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
	  (apply proc (map contents args))
	  (if (= (length args) 2)
	      (let ((type1 (car type-tags))
		    (type2 (cadr type-tags))
		    (a1 (car args))
		    (a2 (cadr args)))
		(let ((t1->t2 (get-coercion type1 type2))
		      (t2->t1 (get-coercion type2 type1)))
		  (cond (t1->t2
			 (apply-generic op (t1->t2 a1) a2))
			(t2->t1
			 (apply-generic op a1 (t2->t1 a2)))
			(else
			 (error "No method for these types"
				(list op type-tags))))))
	      (error "No method for these types"
		     (list op type-tags)))))))

;; a) If apply-generic is called with an operation on two
;; arguments of a type for which there's no such operation
;; in a table, apply-generic will loop infinitely. With
;; Louis' identity function which is installed in the coercion
;; table, the apply-generic will call itself with the same
;; arguments over and over again.

;; b) Louis is wrong that something had to be done about
;; the coercion of arguments of the same type, because
;; it's not what coercion is about. I's supposed
;; to handle arguments of different types. Apply-generic
;; works correctly as is.

;; c) ... Hence there's no need for any modifications (?)
;; But if you want it to work the Louis' way, you
;; have to add the following at the beginning of the last
;; conditional:
;; ((eq? type1 type2)
;;  (error "Coercion on the same types is forbidden"))
