;; From section 3.3.3 for section 2.4.3
;; to support operation/type table for data-directed dispatch

(define (assoc key records)
  (cond ((null? records) #f)
        ((equal? key (caar records)) (car records))
        (else (assoc key (cdr records)))))

(define (make-table)
  (let ((local-table (list '*table*)))
    (define (lookup key-1 key-2)
      (let ((subtable (assoc key-1 (cdr local-table))))
        (if subtable
            (let ((record (assoc key-2 (cdr subtable))))
              (if record
                  (cdr record)
                  #f))
            #f)))
    (define (insert! key-1 key-2 value)
      (let ((subtable (assoc key-1 (cdr local-table))))
        (if subtable
            (let ((record (assoc key-2 (cdr subtable))))
              (if record
                  (set-cdr! record value)
                  (set-cdr! subtable
                            (cons (cons key-2 value)
                                  (cdr subtable)))))
            (set-cdr! local-table
                      (cons (list key-1
                                  (cons key-2 value))
                            (cdr local-table)))))
      'ok)    
    (define (dispatch m)
      (cond ((eq? m 'lookup-proc) lookup)
            ((eq? m 'insert-proc!) insert!)
            (else (error "Unknown operation -- TABLE" m))))
    dispatch))

(define operation-table (make-table))
(define get (operation-table 'lookup-proc))
(define put (operation-table 'insert-proc!))

(define coercion-table (make-table))
(define get-coercion (coercion-table 'lookup-proc))
(define put-coercion (coercion-table 'insert-proc!))


(define (attach-tag type-tag contents)
  (if (number? contents)
      contents
      (cons type-tag contents)))

(define (type-tag datum)
  (cond ((number? datum) 'scheme-number)
	((pair? datum) (car datum))
	(else
	 (error "Bad tagged datum -- TYPE-TAG" datum))))

(define (contents datum)
  (cond ((number? datum) datum)
	((pair? datum) (cdr datum))
	(else
	 (error "Bad tagged datum -- CONTENTS" datum))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; apply-generic

;;;;;;;;;;;;;;;;;;;;;
;; Raising

(define (install-raise-package)
  ;; internal procedures
  (define (int->rat x) (make-rational x 1))
  (define (rat->real x)
    (define (numer x) (car x))
    (define (denom x) (cdr x))
    (make-real (/ (numer x) (denom x))))
  (define (real->complex x)
    (make-complex-from-real-imag x 0))
  ;; interface for the rest of the system
  (put 'raise 'scheme-number int->rat) 
  (put 'raise 'rational rat->real)
  (put 'raise 'real real->complex)
  'done)

(install-raise-package)

;; Numeric-Tower is a [List-of Symbol]
;; A type higher in the tower is a preceeding element in the list
(define num-tower '(complex real rational scheme-number))

;; Symbol Symbol -> Symbol
;; Chooses a higher type from two
(define (max-type type1 type2)
  (let ((t1 (memq type1 num-tower))
	(t2 (memq type2 num-tower)))
    (if (and (pair? t1) (pair? t2))
	(let ((t1-level (length t1))
	      (t2-level (length t2)))
	  (if (= (max t1-level t2-level) t1-level)
	      type1
	      type2))
	(error "MAX-TYPE"
	       "Non-tower types"
	       (list type1 type2 num-tower)))))

;; [List-of Symbol] -> Symbol
(define (highest-type list)
  (define (last list)
    (if (null? (cdr list))
	(car list)
	(last (cdr list))))
  (fold-right (lambda (t rst) (max-type t rst))
	       (last num-tower)
	       list))

;; Object -> [Maybe Object]
;; Raises the object one level in a tower
(define (raise x)
  (let ((candidate (get 'raise (type-tag x))))
    (if candidate
	(candidate (contents x))
	(error "RAISE"
	       "No method for this object"
	       x))))

;; Object Symbol -> Object
;; raises the object to a type specified
;; by a to-tag
(define (successive-raise from to)
  (if (eq? (type-tag from) to)
      from
      (successive-raise (raise from) to)))


;;;;;;;;;;;;;;;;;;;;;
;; Dropping


(define (install-equality-package)
  ;; internal procedures
  (define (=rational? x y)
    (define (numer x) (car x))
    (define (denom x) (cdr x))
    (and (= (numer x) (numer y)) 
	 (= (denom x) (denom y))))
  (define (=complex? x y)
    (and (= (real-part x) (real-part y))
	 (= (imag-part x) (imag-part y))))
  ;; interface for the rest of the system
  (put 'equal? '(scheme-number scheme-number) =)
  (put 'equal? '(rational rational) =rational?)
  (put 'equal? '(real real) =)
  (put 'equal? '(complex complex) =complex?)
  'done)

(install-equality-package)

(define (equ? x y)
  (if (eq? (type-tag x) (type-tag y))
      ((get 'equal? (map type-tag (list x y)))
       (contents x)
       (contents y))
      #f))


(define (install-projection-package)
  ;; internal procedures
  (define (complex->real x)
    (make-real (real-part x)))
  (define (real->rational x)
    (if (inexact? x)
	(make-rational (exact (round x)) 1)
	(make-rational (numerator x)
		       (denominator x))))
  (define (rational->scheme-num x)
    (define (numer x) (car x))
    (numer x))
  ;; interface for the rest of the system
  (put 'project 'rational rational->scheme-num)
  (put 'project 'real real->rational)
  (put 'project 'complex complex->real)
  'done)

(install-projection-package)

;; projects down a number one level in a tower
(define (project x)
  (let ((candidate (get 'project (type-tag x))))
    (if candidate
	(candidate (contents x))
	#f)))


;; drops a number as far as possible in the tower
(define (drop x)
  (let ((candidate (project x)))
    (if candidate
	(if (equ? (raise candidate) x)
	    (drop candidate)
	    x)
	x)))

;; The only issue with this version is that
;; if some type is not present in the table
;; MAX-TYPE will signal an error about it and
;; not APPLY-GENERIC
(define (apply-generic op . args)
  (define (helper)
    (let ((type-tags (map type-tag args)))
      (let ((proc (get op type-tags)))
	(if proc
	    (apply proc (map contents args))
	    (let ((to-type (highest-type type-tags)))
	      (let ((new-args
		     (map (lambda (a) (successive-raise a to-type)) args)))
		(let ((new-proc (get op (map type-tag new-args))))
		  (if new-proc
		      (apply new-proc (map contents new-args))
		      (error "APPLY-GENERIC"
			     "No method for these types"
			     (list op type-tags))))))))))
  (let ((result (helper)))
    (if (pair? result)
	(drop result)
	result)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Generic operations

(define (add x y) (apply-generic 'add x y))
(define (sub x y) (apply-generic 'sub x y))
(define (mul x y) (apply-generic 'mul x y))
(define (div x y) (apply-generic 'div x y))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ordinary numbers

(define (install-scheme-number-package)
  (put 'add '(scheme-number scheme-number) +)
  (put 'sub '(scheme-number scheme-number) -)
  (put 'mul '(scheme-number scheme-number) *)
  (put 'div '(scheme-number scheme-number) /)
  'done)

(install-scheme-number-package)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Rationals

(define (install-rational-package)
  ;; internal procedures
  (define (numer x) (car x))
  (define (denom x) (cdr x))
  (define (gcd a b)
    (if (= b 0)
	a
	(gcd b (remainder a b))))
  (define (make-rat n d)
    (let ((g (gcd n d)))
      (cons (/ n g) (/ d g))))
  (define (add-rat x y)
    (make-rat (+ (* (numer x) (denom y))
		 (* (numer y) (denom x)))
	      (* (denom x) (denom y))))
  (define (sub-rat x y)
    (make-rat (- (* (numer x) (denom y))
		 (* (numer y) (denom x)))
	      (* (denom x) (denom y))))
  (define (mul-rat x y)
    (make-rat (* (numer x) (numer y))
	      (* (denom x) (denom y))))
  (define (div-rat x y)
    (make-rat (* (numer x) (denom y))
	      (* (denom x) (numer y))))
  ;; interface for the rest of the system
  (define (tag x) (attach-tag 'rational x))
  (put 'add '(rational rational)
       (lambda (x y) (tag (add-rat x y))))
  (put 'sub '(rational rational)
       (lambda (x y) (tag (sub-rat x y))))
  (put 'mul '(rational rational)
       (lambda (x y) (tag (mul-rat x y))))
  (put 'div '(rational rational)
       (lambda (x y) (tag (div-rat x y))))
  (put 'make 'rational
       (lambda (n d) (tag (make-rat n d))))
  'done)

(define (make-rational n d)
  ((get 'make 'rational) n d))

(install-rational-package)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Real numbers


;; Suppose that I will use Scheme's internal
;; representation of exact and inexact numbers
;; for my system's representation of real numbers.
(define (install-real-package)
  ;; internal procedures
  (define (make-real x) x)
  ;; interface for the rest of the system
  (define (tag x) (cons 'real x)) ;; I don't want to once again modify attach-tag...
  (put 'add '(real real)
       (lambda (x y) (tag (+ x y))))
  (put 'sub '(real real)
       (lambda (x y) (tag (- x y))))
  (put 'mul '(real real)
       (lambda (x y) (tag (* x y))))
  (put 'div '(real real)
       (lambda (x y) (tag (/ x y))))
  (put 'make 'real
       (lambda (x) (tag (make-real x))))
  'done) 

(install-real-package)

(define (make-real x) ((get 'make 'real) x))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Complex numbers

(define (square x) (* x x))

(define (install-rectangular-package)
  ;; internal procedures
  (define (real-part z) (car z))
  (define (imag-part z) (cdr z))
  (define (make-from-real-imag x y) (cons x y))
  (define (magnitude z)
    (sqrt (+ (square (real-part z))
             (square (imag-part z)))))
  (define (angle z)
    (atan (imag-part z) (real-part z)))
  (define (make-from-mag-ang r a) 
    (cons (* r (cos a)) (* r (sin a))))
  ;; interface to the rest of the system
  (define (tag x) (attach-tag 'rectangular x))
  (put 'real-part '(rectangular) real-part)
  (put 'imag-part '(rectangular) imag-part)
  (put 'magnitude '(rectangular) magnitude)
  (put 'angle '(rectangular) angle)
  (put 'make-from-real-imag 'rectangular
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'rectangular
       (lambda (r a) (tag (make-from-mag-ang r a))))
  'done)

(install-rectangular-package)

(define (install-polar-package)
  ;; internal procedures
  (define (magnitude z) (car z))
  (define (angle z) (cdr z))
  (define (make-from-mag-ang r a) (cons r a))
  (define (real-part z)
    (* (magnitude z) (cos (angle z))))
  (define (imag-part z)
    (* (magnitude z) (sin (angle z))))
  (define (make-from-real-imag x y)
    (cons (sqrt (+ (square x) (square y)))
          (atan y x)))
  ;; interface to the rest of the system
  (define (tag x) (attach-tag 'polar x))
  (put 'real-part '(polar) real-part)
  (put 'imag-part '(polar) imag-part)
  (put 'magnitude '(polar) magnitude)
  (put 'angle '(polar) angle)
  (put 'make-from-real-imag 'polar
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'polar
       (lambda (r a) (tag (make-from-mag-ang r a))))
  'done)

(install-polar-package)


(define (real-part z) (apply-generic 'real-part z))
(define (imag-part z) (apply-generic 'imag-part z))
(define (magnitude z) (apply-generic 'magnitude z))
(define (angle z) (apply-generic 'angle z))


(define (install-complex-package)
  ;; imported procedures from rectangular and polar packages
  (define (make-from-real-imag x y)
    ((get 'make-from-real-imag 'rectangular) x y))
  (define (make-from-mag-ang r a)
    ((get 'make-from-mag-ang 'polar) r a))
  ;; internal procedures
  (define (add-complex z1 z2)
    (make-from-real-imag (+ (real-part z1) (real-part z2))
			 (+ (imag-part z1) (imag-part z2))))
  (define (sub-complex z1 z2)
    (make-from-real-imag (- (real-part z1) (real-part z2))
			 (- (imag-part z1) (imag-part z2))))
  (define (mul-complex z1 z2)
    (make-from-mag-ang (* (magnitude z1) (magnitude z2))
		       (+ (angle z1) (angle z2))))
  (define (div-complex z1 z2)
    (make-from-mag-ang (/ (magnitude z1) (magnitude z2))
		       (- (angle z1) (angle z2))))
  ;; interface for the rest of the system
  (define (tag z) (attach-tag 'complex z))
  (put 'add '(complex complex)
       (lambda (z1 z2) (tag (add-complex z1 z2))))
  (put 'sub '(complex complex)
       (lambda (z1 z2) (tag (sub-complex z1 z2))))
  (put 'mul '(complex complex)
       (lambda (z1 z2) (tag (mul-complex z1 z2))))
  (put 'div '(complex complex)
       (lambda (z1 z2) (tag (div-complex z1 z2))))
  (put 'make-from-real-imag 'complex
       (lambda (x y) (tag (make-from-real-imag x y))))
  (put 'make-from-mag-ang 'complex
       (lambda (r a) (tag (make-from-mag-ang r a))))
  (put 'real-part '(complex) real-part)
  (put 'imag-part '(complex) imag-part)
  (put 'magnitude '(complex) magnitude)
  (put 'angle '(complex) angle)
  'done)

(install-complex-package)

(define (make-complex-from-real-imag x y)
  ((get 'make-from-real-imag 'complex) x y))

(define (make-complex-from-mag-ang r a)
  ((get 'make-from-mag-ang 'complex) r a))
