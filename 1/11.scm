;; a recursive process
(define (f n)
  (if (< n 3)
      n
      (+ (f (- n 1))
	 (* (f (- n 2)) 2)
	 (* (f (- n 3)) 3))))

;; an iterative process
(define (g n)
  (define (iter a b c count)
    (if (< count 3)
	a
	(iter (+ a (* b 2) (* c 3))
	      a
	      b
	      (- count 1))))
  (iter 2 1 0 n))

