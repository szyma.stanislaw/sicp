;; Number N -> Number
;; iteratively computes b times a, using
;; logarithmic no. of steps

(define (* a b)
  (define (double x)
    (+ x x))
  (define (halve x)
    (/ x 2))
  (define (even? n)
    (= (remainder n 2) 0))
  (define (iter a b product)
    (cond ((= b 0) product)
	  ((even? b) (iter (double a) (halve b) product))
	  (else (iter a (- b 1) (+ product a)))))
  (iter a b 0))
