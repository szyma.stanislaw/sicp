(load "82_multi_coercions.scm")

(define (install-zero-package)
  ;; internal procedures
  (define (zero-rational? x)
    (define (numer x) (car x))
    (zero? (numer x)))
  (define (zero-complex? z)
    (= (real-part z) (imag-part z) 0))
  (define (zero-poly? p)
    (define (term-list p) (cdr p))
    (null? (term-list p))) 
  ;; interface for the rest of the system
  (put 'zero? '(scheme-number) zero?)
  (put 'zero? '(rational) zero-rational?)
  (put 'zero? '(complex) zero-complex?)
  (put 'zero? '(polynomial) zero-poly?)
  'done)

(install-zero-package)

(define (=zero? x) (apply-generic 'zero? x))

;; this solution is thanks to qiao from github
;; Our new polynomial representation is as follows:
;; (make-polynomial [List-of Variable] [List-of Term])
;; [List-of Variable] stores information about every
;; variable that is present in the polynomial. By
;; this assumption it is unnecessary to, e.g. to have
;; a polynomial in x, whose coefficients are
;; polynomials in y, z, etc. We just expand it.
;; But I must devise a way to expand it (To make it easier for
;; myself to implement it, I will first consider the polynomials
;; as already expanded.)
;;
;; In addition and multiplication I must be able to compare
;; two lists of Variables and if one of them does not have
;; a variable that the other does, then we use the one that
;; has more of them. They will be in an alphabetical order,
;; because for our purposes 23 letters is more than enough.
;; We will use symbol->string and string<? procedures.

;; Term is represented as
;; (make-term Orders Coefficient)
;; (make-term [List-of Number] Number)
;; where the list of Coefficients determines the coefficients of
;; every variable that a polynomial contains, in the same
;; order as described in a list of Variables in make-polynomial
;; procedure.
;;
;; I must be able to somehow represent (x^5)(y^2) + (y)(z^6)

;; [List-of Variable] -> Boolean
;;(define (vars-in-order? vars)
;;  (apply string<? (map symbol->string vars)))

;; Symbol [List-of Variable] -> Boolean
;;(define (member? s los)
;;  (cond ((null? lost) #f)
;;	((eq? s (car los)) #t)
;;	(else (member? s (cdr los)))))

;; [List-of Variable] -> [List-of Variable]
;;(define (remove-duplicates los)
;;  (let ((first (car los))
;;	 (rest (cdr los)))
;;    (cond ((null? los) '())
;;	  ((member? first rest)
;;	   (remove-duplicates (remove first rest)))
;;	  (else (cons first (remove-duplicates rest))))))

;; [List-of Variable] [List-of Variable] -> [List-of Variable]
;;(define (new-vars x y)
;;  (if (and (vars-in-order? x) (vars-in-order? y))
;;      (remove-duplicates (append x y))
;;      (error "NEW-VARS" "VARS NOT IN ORDER" x y)))

   
(define (install-polynomial-package)
  ;; internal procedures
  ;; representation of poly
  ;; [List-of Symbol] [List-of Term] -> Poly
  (define (make-poly var-list term-list) (cons var-list term-list))
  (define (var-list p) (car p))
  (define (term-list p) (cdr p))
  (define (var-list? x)
    (fold-right (lambda (s rst) (and (symbol? s) rst))
		#t
		x))
  (define (same-var-list? x y)
    (fold-right (lambda (a b rst) (and (eq? a b) rst))
		#t
		x
		y))
  ;; representation of terms and term lists
  ;; As most polynomial manipulations are performed on
  ;; sparse polynomials, whose majority of coefficients is
  ;; zero, a term is a list containing an order of that term
  ;; and its coefficient. A term list is arranged from highest
  ;; to lowest-order term.
  (define (the-empty-termlist) '())
  (define (first-term term-list) (car term-list))
  (define (rest-terms term-list) (cdr term-list))
  (define (empty-termlist? term-list) (null? term-list))

  (define (make-term order-list coeff) (list order-list coeff))
  (define (order-list term) (car term))
  (define (coeff term) (cadr term))
  
  (define (adjoin-term term term-list)
    (if (=zero? (coeff term))
	term-list
	(cons term term-list)))

  ;; polynomial manipulations
  (define (add-poly p1 p2)
    (if (same-var-list? (var-list p1) (var-list p2))
	(make-poly (var-list p1)
		   (add-terms (term-list p1) (term-list p2)))
	(error "ADD-POLY" "Polys not in same var-list" (list p1 p2))))
  (define (add-terms L1 L2)
    (cond ((empty-termlist? L1) L2)
	  ((empty-termlist? L2) L1)
	  (else
	   (let ((t1 (first-term L1)) (t2 (first-term L2)))
	     (cond ((> (order t1) (order t2))
		    (adjoin-term
		     t1 (add-terms (rest-terms L1) L2)))
		   ((< (order t1) (order t2))
		    (adjoin-term
		     t2 (add-terms L1 (rest-terms L2))))
		   (else
		    (adjoin-term
		     (make-term (order t1)
				(add (coeff t1) (coeff t2)))
		     (add-terms (rest-terms L1)
				(rest-terms L2)))))))))

  (define (mul-poly p1 p2)
    (if (same-variable? (variable p1) (variable p2))
	(make-poly (variable p1)
		   (mul-terms (term-list p1) (term-list p2)))
	(error "MUL-POLY" "Polys no in same var" (list p1 p2))))
  (define (mul-terms L1 L2)
    (if (empty-termlist? L1)
	(the-empty-termlist)
	(add-terms (mul-term-by-all-terms (first-term L1) L2)
		   (mul-terms (rest-terms L1) L2))))
  (define (mul-term-by-all-terms t1 L)
    (if (empty-termlist? L)
	(the-empty-termlist)
	(let ((t2 (first-term L)))
	  (adjoin-term
	   (make-term (+ (order t1) (order t2))
		      (mul (coeff t1) (coeff t2)))
	   (mul-term-by-all-terms t1 (rest-terms L))))))
		    
  ;; interface for the rest of the system
  (define (tag p) (attach-tag 'polynomial p))
  (put 'add '(polynomial polynomial)
       (lambda (p1 p2) (tag (add-poly p1 p2))))
  (put 'mul '(polynomial polynomial)
       (lambda (p1 p2) (tag (mul-poly p1 p2))))
  (put 'make 'polynomial
       (lambda (var terms) (tag (make-poly var terms))))
'done)

(install-polynomial-package)

(define (make-polynomial var terms)
  ((get 'make 'polynomial) var terms))

