;; [List-of Number] -> [List-of Number]
;; returs a list of all the arguments that have
;; the same even-odd parity as the first argument
(define (same-parity a . b)
  (define (try list)
    (let ((parity? (if (even? a) even? odd?)))
      (cond ((null? list) list)
	    ((parity? (car list))
	     (cons (car list) (try (cdr list))))
	    (else (try (cdr list))))))
  (cons a (try b)))

(newline)
(display (same-parity 1 2 3 4 5 6 7))
(newline)
(display (same-parity 2 3 4 5 6 7))
   
