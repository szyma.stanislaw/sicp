;; Interval is a structure
(define (make-interval a b) (cons a b))
;; where a and b represent the lower and
;; the upper bound of an interval

;; selectors
(define (lower-bound i) (car i))
(define (upper-bound i) (cdr i))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
		 (+ (upper-bound x) (upper-bound y))))

(define (mul-interval x y)
  (let ((a (lower-bound x))
	(b (upper-bound x))
	(c (lower-bound y))
	(d (upper-bound y)))
    (cond ((and (< a b 0) (< c d 0))
	   (make-interval (* b d) (* a c)))
	  ((and (< a b 0) (< c 0 d))
	   (make-interval (* a d) (* a c)))
	  ((and (< a b 0) (< 0 c d))
	   (make-interval (* a d) (* b c)))
	  ((and (< a 0 b) (< c d 0))
	   (make-interval (* b c) (* a c)))
	  ((and (< a 0 b) (< c 0 d)) ;; the one 
	   (make-interval (min (* b c) (* a d))
			  (max (* a c) (* b d))))
	  ((and (< a 0 b) (< 0 c d))
	   (make-interval (* a d) (* b d)))
	  ((and (< 0 a b) (< c d 0))
	   (make-interval (* b c) (* a d)))
	  ((and (< 0 a b) (< c 0 d))
	   (make-interval (* b c) (* b d)))
	  ((and (< 0 a b) (< 0 c d))
	   (make-interval (* a c) (* b d))))))

(define (div-interval x y)
  (let ((lowy (lower-bound y))
	(upy (upper-bound y)))
    (if (or (= lowy 0) (= upy 0))
	(error "div-internal: division by zero")
	(mul-interval x (make-interval (/ 1.0 upy)
				       (/ 1.0 lowy))))))

(define (sub-interval x y)
  (make-interval (- (lower-bound x) (upper-bound y))
		 (- (upper-bound x) (lower-bound y))))
