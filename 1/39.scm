;; an approximation of a continued fraction
;; representation of a tangent function due
;; to J.H. Lambert, for x in radians
;; k = 100 recommended
(define (tan-cf x k)
  (cont-frac (lambda (i) (if (= i 1)
			     x
			     (square x)))
	     (lambda (i) (- (* 2 i) 1))
	     k))

;; looks like it works
(newline)
(display (tan 52.0))
(newline)
(display (tan-cf 52.0 100)) ;; it converges very slowly
(newline)
	     

;; [X -> Number] [X -> Number] Number -> Number
;; approximates an infinite continued fraction
;; until kth term
(define (cont-frac n d k)
  (define (continue count)
    (if (> count k)
	0
	(/ (n count)
	   (- (d count)
	      (continue (+ count 1))))))
  (continue 1))
