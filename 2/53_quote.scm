(newline)
(display (list 'a 'b 'c)) ;; (a b c)
(newline)
(display (list (list 'george))) ;; ((george))
(newline)
(display (cdr '((x1 x2) (y1 y2)))) ;; ((y1 y2))
(newline)
(display (cadr '((x1 x2) (y1 y2)))) ;; (y1 y2)
(newline)
(display (pair? (car '(a short list)))) ;; #f

;; Symbol [List-of Symbol] -> [List-of Symbol] or Boolean
;; if a symbol is present in a list, it prints out
;; a sublist of the list beginning with the first
;; occurence of that symbol, otherwise false
(define (memq item list)
  (cond ((null? list) #f)
	((eq? (car list) item) list)
	(else (memq item (cdr list)))))

(newline)
(display (memq '(this is a list) '(this is a list))) ;; #f
(newline)
(display (memq 'red '(red shoes blue socks))) ;; (red shoes blue socks)
