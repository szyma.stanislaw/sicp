;; [N -> N] N -> [N -> N]
;; given procedure f produces
;; f(f(...(f(f(x)))...)) n times,
;; where n is a positive integer
(define (repeated f n)
  (define (compose f g)
    (lambda (x) (f (g x))))
  (if (= n 1)
      f
      (compose f (repeated f (- n 1)))))

(newline)
(display ((repeated square 2) 5))
(newline)


