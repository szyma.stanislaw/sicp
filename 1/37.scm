;; [X -> Number] [X -> Number] Number -> Number
;; approximates an infinite continued fraction
;; until kth term
(define (cont-frac n d k)
  (define (continue count)
    (if (> count k)
	0
	(/ (n count)
	   (+ (d count)
	      (continue (+ count 1))))))
  (continue 1))


;; a reciprocal of the golden ratio
(newline)
(display (/ 2.0 (+ 1 (sqrt 5))))

;; its approximation accurate to 4 decimal places 
(newline)
(display (cont-frac (lambda (i) 1.0)
		    (lambda (i) 1.0)
		    11))



;; an iterative version of cont-frac
(define (cont-frac-iter n d k)
  ;; we reverse the order and start
  ;; counting from k until 0
  (define (continue count result)
    (if (= count 0)
	result
	(continue (- count 1)
		  (/ (n count)
		     (+ (d count) result)))))
  (continue (- k 1) (/ (n k)
		       (d k))))

;; works as well
(newline)
(display (cont-frac-iter (lambda (i) 1.0)
			 (lambda (i) 1.0)
			 11))
(newline)
