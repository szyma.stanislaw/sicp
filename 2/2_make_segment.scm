;; Point is a structure
(define (make-point x y) (cons x y))
;; (make-point Number Number)
;; x and y are real numbers that represent
;; x and y-coordinate of a point

;; examples
(define A (make-point 1.0 1.0))
(define B (make-point 4.0 10.0))
(define C (make-point -4.0 24.0))
(define D (make-point 15.0 -5.6))

;; selectors
(define (x-point p) (car p))
(define (y-point p) (cdr p))


;; Segment is a structure
(define (make-segment p q) (cons p q))
;; (make-segment Point Point)
;; p and q denote two points that represent
;; line segments in a plane

;; example
(define AB (make-segment A B))
(define CD (make-segment C D))

;; selectors
(define (start-segment s) (car s))
(define (end-segment s) (cdr s))


;; Segment -> Point
;; computes the midpoint of a given segment
(define (midpoint s)
  (define (average a b) (/ (+ a b) 2))
  (let ((A (start-segment s))
	(B (end-segment s)))
    (make-point (average (x-point A) (x-point B))
		(average (y-point A) (y-point B)))))

;; nicely prints out point's coordinates
(define (print-point p)
  (newline)
  (display "(")
  (display (x-point p))
  (display ",")
  (display (y-point p))
  (display ")"))

