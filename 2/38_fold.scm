(define (fold-left op init seq)
  (define (iter result rest)
    (if (null? rest)
	result
	(iter (op result (car rest))
	      (cdr rest))))
  (iter init seq))

(newline)
(display (fold-right / 1 '(1 2 3))) ;; 3/2
(newline)
(display (fold-left / 1 '(1 2 3))) ;; 1/6
(newline)
(display (fold-right list '() '(1 2 3))) ;; '(1 (2 (3 ())))
(newline)
(display (fold-left list '() '(1 2 3))) ;; '(((() 1) 2) 3)

;; fold-right and fold-left will produce the same value for a
;; sequence if and only if the combiner op satisfies the
;; commutative property, i.e, changing the order of operands
;; does not change the result.
