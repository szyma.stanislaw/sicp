;; Tree -> Tree
;; squares every leave of a tree of numbers
(define (square-tree tree)
  (map (lambda (sub-tree)
	 (if (pair? sub-tree)
	     (square-tree sub-tree)
	     (square sub-tree)))
       tree))

(define x (list 1 (list 2 (list 3 4) 5) (list 6 7)))
(newline)
(display (square-tree x))
