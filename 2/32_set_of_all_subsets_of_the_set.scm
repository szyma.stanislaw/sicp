(define (subsets s)
  (if (null? s)
      (list '())
      (let ((rest (subsets (cdr s))))
	(append rest (map (lambda (subset)
			    (cons (car s) subset))
			  rest)))))

(define x '(1 2 3))
(newline)
(display (subsets x))

;; We merge two lists together: one in which
;; there are all subsets without the first element,
;; and one in which there are all subsets with it.
