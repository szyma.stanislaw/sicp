;; looking at primes and their elapsed times
;; necessary to pass the primality test

;; smallest primes greater than 10^10 
;; 10000000019 *** .08999999999999986
;; 10000000033 *** .08999999999999986
;; 10000000061 *** .08999999999999986

;; smallest primes greater than 10^12
;; 1000000000039 *** .9100000000000001
;; 1000000000061 *** .9199999999999999
;; 1000000000063 *** .9199999999999999

;; smallest primes greater than 10^14
;; 100000000000031 *** 9.14
;; 100000000000067 *** 9.120000000000001
;; 100000000000097 *** 9.29

;; smallest primes greater than 10^16
;; 10000000000000061 *** 91.24000000000001
;; 10000000000000069 *** 91.69
;; 10000000000000079 *** 91.21999999999997

;; We see that if we test for primality a 100 times
;; larger number than the previous one, we increase
;; around square root of 100 or 10 times the number
;; of steps required to satisfy that test. This
;; supports the order of growth of prime? predicate
;; which is square root of n.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; computes a^b
;; I am looking for large primes
(define (exp a b)
  (define (iter a b product)
    (cond ((= b 0) product)
	  ((even? b)
	   (iter (square a) (/ b 2) product))
	  (else
	   (iter a (- b 1) (* product a)))))
  (iter a b 1))

;; if the procedure stumbles upon a prime it
;; in [a, b[ interval it embeds to it elapsed
;; time required to pass the primality test
(define (search-for-primes a b)
  (define (odd-iter lower)
    (cond ((>= lower b) b)
	  (else (timed-prime-test lower)
		(odd-iter (+ lower 2)))))
  (odd-iter (if (not (even? a))
		a
		(+ a 1))))

(define (even? n)
  (= (remainder n 2) 0))

;; if n is prime it prints n and shows after three asterisks
;; elapsed time needed to check it, otherwise only prints n
(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

;; N -> Boolean
;; is n prime
(define (prime? n)
  (= n (smallest-divisor n)))

(define (smallest-divisor n)
  (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
	((divides? test-divisor n) test-divisor)
	(else (find-divisor n (+ test-divisor 1)))))

(define (divides? a b)
  (= (remainder b a) 0))
