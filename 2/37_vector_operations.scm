;; Vector is a sequence of numbers
;; Matrix is a sequence of vectors (rows)

;; Vector Vector -> Number
(define (dot-product v w)
  (accumulate + 0 (map * v w)))

;; Matrix Vector -> Vector
(define (matrix-*-vector m v)
  (map (lambda (row) (dot-product row v)) m))

;; Matrix -> Matrix
(define (transpose mat)
  (accumulate-n cons '() mat))

;; Matrix Matrix -> Matrix
(define (matrix-*-matrix m n)
  (let ((cols (transpose n)))
    (map (lambda (row) (matrix-*-vector cols row)) ;; I can't imagine doing this exercise without Wikipedia
	 m)))


(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      '()
      (cons (accumulate op init (map car seqs))
	    (accumulate-n op init (map cdr seqs)))))

(define (accumulate op init seq)
  (if (null? seq)
      init
      (op (car seq)
	  (accumulate op init (cdr seq))))) 

