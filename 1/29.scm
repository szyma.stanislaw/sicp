(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
	 (sum term (next a) next b))))

;; approximates a definite integral of a function f
;; between a and b using Simpson's rule
(define (simpson f a b n)
  (define h (/ (- b a) n))
  (define (y k) (f (+ a (* h k))))
  (define (next n) (+ n 2))
  (* (+ (y 0)
	(y n)
	(* (sum y 2 next (- n 2))
	   2)
	(* (sum y 1 next (- n 1))
	   4))
     (/ h 3.0)))



;; approximates a definite integral of f between a and b
(define (integral f a b dx)
  (define (add-dx x) (+ x dx))
  (* (sum f (+ a (/ dx 2.0)) add-dx b)
     dx))

;; simpson procedure has greater accuracy
(newline)
(display (simpson cube 0 1 100))
(newline)
(display (simpson cube 0 1 1000))
(newline)
(display (integral cube 0 1 .001))
(newline)
