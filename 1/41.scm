;; [N -> N] -> [N -> N]
;; takes a procedure of one argument and
;; returns a procedure that applies the
;; original procedure twice
(define (double f)
  (lambda (x) (f (f x))))


;; (((double (double double)) inc) 5)
;; (((double (lambda (x) (double (double x)))) inc) 5)
;; (((lambda (x) (double (double (double (double x))))) inc) 5)
;; (+ 5 16)
;; 21 

(define (inc x) (+ x 1))


