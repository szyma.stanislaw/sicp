;; Let x denote the golden ratio which satisfies
;; the following equation

;; x^2 = x + 1  |: x
;; x = 1 + 1/x

;; Therefore golden ratio is a fixed point of a
;; function f(x) = 1 + 1/x, so it can be written
;; in terms of fixed-point procedure as

(newline)
(display (fixed-point (lambda (x) (+ 1 (/ 1.0 x))) 1.0))
(newline)

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
	  next
	  (try next))))
  (try first-guess))

(define tolerance 0.00001)
