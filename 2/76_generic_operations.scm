;; In generic operations with explicit dispatch,
;; when we create a new type, we have to first
;; create all the appropriate selectors and
;; constructors, and then rewrite the existing
;; generic operations to include that type.
;; With high number of types name conflicts
;; may occur. In case of adding new generic
;; operations, it must include all the types
;; present in the system and their respective
;; functions. A lot of rewriting.

;; In data-directed style, when adding a type
;; we supply as a package in the (hash ?)table
;; all the non-generic procedures related to it. 
;; When adding a new generic operation we retrieve
;; the required operations by the their tag that
;; is also attached to the processed data.

;; In message-passing style the data is itself
;; a procedure which recieves the requested
;; operation name as a "message". In other words,
;; data is aware of all the types and handles the
;; messages by itself. Creation of a new type is a
;; matter of adding a new constructor, yet creation
;; of a new generic operation is done at cost of rewriting
;; all the constructors to include it.

;; I think it's easier to add new types to a
;; system using either data-directed or message-
;; passing style organization, which require
;; only to add a new package or a constructor.
;; It's easier to add new generic operations
;; using data-directed style, because it's
;; only a matter of retrieving appropriate
;; procedures from the table or in the worst
;; case creating a new package. Data-directed
;; programming seems to be a really flexible
;; approach.
