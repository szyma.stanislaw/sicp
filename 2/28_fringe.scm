;; Tree -> [List-of X]
;; outputs a list of all leaves of a tree
;; in a left-to-right order
(define (fringe tree)
  (cond ((null? tree) tree)
	((not (pair? tree)) (list tree))
	(else (append (fringe (car tree))
		      (fringe (cdr tree))))))

(define x (list (list 1 2) (list 3 4)))

(newline)
(display (fringe x))
(newline)
(display (fringe (list x x)))
