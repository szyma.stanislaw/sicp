;; Since sicp was released, computers got so fast
;; that testing this procedure in mit-scheme makes no
;; sense because the runtime procedure outputs the
;; time in seconds while I am interested in mili- or
;; microseconds. I would expect however that if an
;; input integer is squared, the time necessary to
;; pass the test once would increase twice, because
;; fast-prime? predicate is an O(logn) procedure.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; if the procedure stumbles upon a prime it
;; in [a, b[ interval it embeds to it elapsed
;; time required to pass the primality test
(define (search-for-primes a b)
  (define (odd-iter lower)
    (cond ((>= lower b) b)
	  (else (timed-prime-test lower)
		(odd-iter (+ lower 2)))))
  (odd-iter (if (not (even? a))
		a
		(+ a 1))))

(define (even? n)
  (= (remainder n 2) 0))

;; if n is prime it prints n and shows after three asterisks
;; elapsed time needed to check it, otherwise only prints n
(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (fast-prime? n 1)
      (report-prime (- (runtime) start-time))))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

;; checks n for primality using Fermat's little theorem.
;; the more times the test is evaluated the greater the
;; probability that n is a prime.
(define (fast-prime? n times)
  (cond ((= times 0) #t)
	((fermat-test n) (fast-prime? n (- times 1)))
	(else #f)))

;; performs the expmod procedure on a random
;; nonzero integer that is smaller then the
;; input of this procedure
(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random-integer (- n 1)))))

;; computes exponential of a number modulo another number
(define (expmod base exp m)
  (cond ((= exp 0) 1)
	((even? exp)
	 (remainder (square (expmod base (/ exp 2) m))
		    m))
	(else (remainder (* base (expmod base (- exp 1) m))
			 m))))

