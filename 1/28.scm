;; implementation of Miller-Rabin test
(define (mr-test n)
  (define (try-it a)
    (cond ((= a 1) #t)
	  ((= (expmod a (- n 1) n) 1)
	   (try-it (- a 1)))
	  (else #f)))
  (try-it (- n 1)))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
	((even? exp)
	 (sq-mod-check (expmod base (/ exp 2) m)
		       m))
	(else (remainder (* base (expmod base (- exp 1) m))
			 m))))

(define (sq-mod-check n m)
  (define sq-mod-n
    (remainder (square n) m))
  (if (and (not (or (= n 1)
		    (= n (- m 1))))
	   (= sq-mod-n 1))
      0
      sq-mod-n))

;; Carmichael numbers don't pass it
(display (mr-test 561))
(newline)
(display (mr-test 1105))
(newline)
(display (mr-test 1729))
(newline)
(display (mr-test 2465))
(newline)
(display (mr-test 2821))
(newline)
(display (mr-test 6601))
(newline)
(newline)

;; the rest as usual
(display (mr-test 3))
(newline)
(display (mr-test 19))
(newline)
(display (mr-test 331))
(newline)
(display (mr-test 27))
