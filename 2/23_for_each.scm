;; [X -> Y] [List-of X] -> true
;; applies a procedure to every element in a list,
;; but still returns true
(define (for-each proc list)
  (cond ((null? list) (newline) #t)
	(else
	 (proc (car list))
	 (for-each proc (cdr list)))))

(display (for-each (lambda (x) (newline) (display x))
		   (list 57 321 88)))
