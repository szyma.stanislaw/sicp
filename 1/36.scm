;; the equation
;; x^x = 1000
;; x = 3/logx
;; x is a fixed point of a function f(x) = 3/logx

;; without average-damping final approximation took 257 iterations
(newline)
(display (fixed-point (lambda (x) (/ 3 (log x)))
		       2.0))
(newline)


;; ...with average damping it took 6 iterations
(define (average a b) (/ (+ a b) 2))

(display (fixed-point (lambda (x) (average x (/ 3 (log x))))
		      2.0))
(newline)



(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (cond ((close-enough? guess next)
	     (newline)
	     next)
	    (else
	     (newline)
	     (display next)
	     (try next)))))
  (try first-guess))

(define tolerance 0.00001)
