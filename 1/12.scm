;; N N -> N
;; Computes an element of Pascal's triangle
;; which is in the n-th row and k-th column.
;; Assume that if k > n, then the rightmost
;; value will be produced.
(define (pascal n k)
  (cond ((or (= n 0) (= k 0)) 1) 
	((= n k) 1)
	(else (+ (pascal (- n 1) k)
		 (pascal (- n 1) (- k 1))))))
