;; well enough...
(define (iterative-improve good-enough? improve)
  (define (try guess)
	   (if (good-enough? guess)
	       guess
	       (iter-improve (improve guess))))
  (lambda (x) (try x)))

;; the problem is which guess we decide to choose.
;; In fixed-point procedure we take improved guess
;; as the "good enough" and in sqrt we take the
;; not-yet-improved one.

;; sqrt procedure from 1.1.7
(define (sqrt x)
  (define (good-enough? guess)
    (< (abs (- (square guess) x)) 0.001))
  (define (improve guess)
    (average guess (/ x guess)))
  (define (average a b) (/ (+ a b) 2))
  ((iterative-improve good-enough? improve) 1.0))


;; fixed-point procedure from 1.3.3
(define (fixed-point f)
  (define (close-enough? x)
    (< (abs (- x (f x))) tolerance))
  ((iterative-improve close-enough? f) 1.0))

(define tolerance 0.00001)
