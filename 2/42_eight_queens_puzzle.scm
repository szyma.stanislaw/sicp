;; Position of a queen on the board is denoted as [List N N]
;; Solution is a [List-of Position]

;; N -> [List-of Solution]
;; produces a sequence of all solutions to a problem
;; of placing n queens on a n x n chessboard
(define (queens board-size)
  (define (flatmap proc seq)
    (fold-right append '() (map proc seq)))
  (define (enumerate-interval low high)
    (if (> low high)
	'()
	(cons low (enumerate-interval (+ low 1) high))))
  (define (adjoin-position row col rest-of-queens)
    (cons (list row col) rest-of-queens))
  (define (safe? k positions)
    (define (no-danger? queen1 queen2)
      (let ((x1 (car queen1))
	    (y1 (cadr queen1))
	    (x2 (car queen2))
	    (y2 (cadr queen2)))
	(and (not (= x1 x2)) ;; same row
	     (not (= y1 y2)) ;; same column
	     (not (= (abs (- x1 x2)) ;; same diagonal, "slope"
		     (abs (- y1 y2)))))))
    (define (check-queen queen rest-of-queens) ;; andmap 
      (if (null? rest-of-queens)
	  #t
	  (and (no-danger? queen (car rest-of-queens))
	       (check-queen queen (cdr rest-of-queens)))))
    (check-queen (car positions) (cdr positions))) ;; the first position in the Solution is always the newest one
  (define (queen-cols k)
    (if (= k 0)
	(list '()) ;; empty board
	(filter
	 (lambda (positions) (safe? k positions))
	 (flatmap
	  (lambda (rest-of-queens)
	    (map (lambda (new-row)
		   (adjoin-position new-row k rest-of-queens))
		 (enumerate-interval 1 board-size)))
	  (queen-cols (- k 1))))))
  (queen-cols board-size))
