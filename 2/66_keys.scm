;; Record is some piece of data that is identified by its key.
;; Suppose that the key is a number.

;; Set of records is represented as a binary tree, which
;; must have entry and left and right subtrees.
(define (lookup given-key set-of-records)
  (if (null? set-of-records)
      #f
      (let ((entry-record (entry set-of-records)))
	(let ((entry-key (key entry-record)))
	  (cond ((= given-key entry-key) entry-record)
		((< given-key entry-key)
		 (lookup given-key (left-branch set-of-records)))
		((> given-key entry-key)
		 (lookup given-key (right-branch set-of-records))))))))
  
