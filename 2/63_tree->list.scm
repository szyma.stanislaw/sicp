;; Node is a number

(define (make-tree entry left right)
  (list entry left right))
;; Tree is one of:
;; - '()
;; - Number
;; - [List Node LTree RTree]
;; Constraint:
;; LTree < Node < RTree

;; selectors
(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))


(define (tree->list-1 tree) 
  (if (null? tree)
      '()
      (append (tree->list-1 (left-branch tree))
	      (cons (entry tree)
		    (tree->list-1 (right-branch tree))))))

(define (tree->list-2 tree) 
  (define (copy-to-list tree result-list)
    (if (null? tree)
	result-list
	(copy-to-list (left-branch tree)
		      (cons (entry tree)
			    (copy-to-list (right-branch tree)
					  result-list)))))
  (copy-to-list tree '()))

(define x (make-tree 1 '()
		     (make-tree 2 '()
				(make-tree 3 '()
					   (make-tree 4 '()
						      (make-tree 5 '()
								 (make-tree 6 '()
									    (make-tree 7 '() '()))))))))
(newline)
(display (tree->list-1 x))
(newline)
(display (tree->list-2 x))

;; Both processes grow linearly with time and obtain exactly the same result.
;; The difference is only in the order in which each subtree is evaluated.
