;; [N -> N] -> [N -> N]
;; produces a smoothed version of f
(define (smooth f)
  (define (average a b c)
    (/ (+ a b c) 3))
  (lambda (x)
    (average (f (- x dx))
	     (f x)
	     (f (+ x dx)))))

(define dx 0.00001)


;; [N -> N] N -> [N -> N]
;; produces n-fold smoothed function
;; for any positive integer n
(define (n-fold-smooth f n)
  ((repeated smooth n) f))


(define (repeated f n)
  (define (compose f g)
    (lambda (x) (f (g x))))
  (if (= n 1)
      f
      (compose f (repeated f (- n 1)))))



