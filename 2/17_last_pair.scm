;; [List-of X] -> X
;; returs the last element of a nonempty list
(define (last-pair list)
  (if (null? (cdr list))
      (car list)
      (last-pair (cdr list))))

(newline)
(display (last-pair (list 23 72 149 34)))
