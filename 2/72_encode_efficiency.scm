;; Symbol Tree -> [Maybe Bits]
(define (encode-symbol s t)
  (if (leaf? t)
      '()
      (let ((left (left-branch t))
	    (right (right-branch t)))
	(cond ((member? s (symbols left))
	       (cons 0
		     (encode-symbol s left)))
	      ((member? s (symbols right))
	       (cons 1
		     (encode-symbol s right)))
	      (else
	       (error "no symbol in tree -- ENCODE-SYMBOL" symbol))))))

(define (member? symbol list)
  (cond ((null? list) #f)
	((eq? (car list) symbol) #t)
	(else (member? symbol (cdr list))))) 

;; Answer to this question for the special case of the
;; tree from ex. 71, where the relative frequencies of
;; n symbols were 1, 2, 4, 8,..., 2^{n-1}, mostly depends
;; on whether the symbols with smaller relative
;; frequencies are in the left or right branch of the tree,
;; because the key element of our Huffman tree structure is
;; a list of all elements present in it which is the result
;; of appending its left and right branches respectively.
;; Thus, assuming that the less frequent symbols are present
;; in the left branch, the order of growth of the number of
;; steps required to encode the most frequent symbol is O(n),
;; because first left branch is evaluated with n-1 elements, and
;; then the right one with only one element. In case of the
;; least frequent symbol the time complexity is also O(n), because
;; it will always appear at the beginning of the list at all
;; n-1 levels of the tree. 
