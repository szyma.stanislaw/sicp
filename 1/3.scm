;; Number Number Number -> Number
;; returns the sum of the squares of the two larger numbers
(define (f a b c)
  (define (sum-of-squares x y)
    (+ (square x) (square y)))
  (if (> a b)
      (sum-of-squares a (if (> b c) b c))
      (sum-of-squares b (if (> a c) a c))))
		 
