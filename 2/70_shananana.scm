(define (make-leaf symbol weight)
  (list 'leaf symbol weight))

(define (leaf? object)
  (eq? (car object) 'leaf))

(define (symbol-leaf x) (cadr x))
(define (weight-leaf x) (caddr x))


(define (make-code-tree left right)
  (list left
	right
	(append (symbols left) (symbols right))
	(+ (weight left) (weight right))))

(define (left-branch tree) (car tree))
(define (right-branch tree) (cadr tree))

(define (symbols tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))

(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (adjoin-set x set)
  (cond ((null? set) (list x))
	((< (weight x) (weight (car set))) (cons x set))
	(else (cons (car set)
		    (adjoin-set x (cdr set))))))

(define (make-leaf-set pairs)
  (if (null? pairs)
      '()
      (let ((pair (car pairs)))
	(adjoin-set (make-leaf (car pair)   ;; symbol
			       (cadr pair)) ;; frequency
		    (make-leaf-set (cdr pairs))))))

;; Pairs -> Tree
(define (generate-huffman-tree pairs)
  (succesive-merge (make-leaf-set pairs)))

;; Leaf -> Tree
(define (succesive-merge set)
  (if (null? (cdr set))
      (car set)
      (let ((new-tree (make-code-tree (car set) (cadr set)))
	    (rest (cddr set)))
	(succesive-merge (adjoin-set new-tree rest))))) ;; use adjoin-set

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; [List-of Symbol] Tree -> [Maybe Bits]
(define (encode message tree)
  (if (null? message)
      '()
      (append (encode-symbol (car message) tree)
	      (encode (cdr message) tree))))

;; Symbol Tree -> [Maybe Bits]
(define (encode-symbol s t)
  (if (leaf? t)
      '()
      (let ((left (left-branch t))
	    (right (right-branch t)))
	(cond ((member? s (symbols left))
	       (cons 0
		     (encode-symbol s left)))
	      ((member? s (symbols right))
	       (cons 1
		     (encode-symbol s right)))
	      (else
	       (error "no symbol in tree -- ENCODE-SYMBOL" s))))))

(define (member? symbol list)
  (cond ((null? list) #f)
	((eq? (car list) symbol) #t)
	(else (member? symbol (cdr list))))) 


(define rock-alphabet '((A 2) (BOOM 1) (GET 2) (JOB 2) (NA 16) (SHA 3) (YIP 9) (WAH 1)))

(define rock-tree (generate-huffman-tree rock-alphabet))

(define rock-song '(Get a job Sha na na na na na na na na Get a job
		    Sha na na na na na na na na
		    Wah yip yip yip yip yip yip yip yip yip
		    Sha boom))

(define rock-bits (encode rock-song rock-tree))

;; Encoded rock song (rock-bits) requires 84 bits. Fixed-length code using
;; eight-symbol alphabet would require every symbol to consist of 3 bits,
;; and the rock-song consists of 36 such symbols yielding in total 108 bits.
