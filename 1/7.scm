(define (sqrt x)
  (sqrt-iter 1.0 x))

(define (sqrt-iter guess x)
  (define (good-enough? guess x)
    (< (abs (- (square guess) x)) 0.001))
  (define (improve guess x)
    (average guess (/ x guess)))
  (define (average x y)
    (/ (+ x y) 2))
  (if (good-enough? guess x)
      guess
      (sqrt-iter (improve guess x) x)))

;; (square (sqrt-iter 1.0 0.000005))
;; Value: 9.798942834097074e-4

;; (square (sqrt-iter 1.0 999393939393249829))
;; Value: 999393939393249800

;; good-enough? test fails for numbers smaller than or
;; of equal order of magnitude as the given tolerance
;; value (0.001), because a guess of a square root of
;; such a number does not have to be accurate to pass
;; that test. The limited precision of arithmetic
;; operations during many iterations of the sqrt-iter
;; algorithm leads to less and less accurate results.
;; The larger the number, the more iterations are
;; necessary to pass the test, the smaller the precision.

;; a refinement of sqrt
(define (sqrt2 x)
  (define (improve guess)
    (average guess (/ x guess)))
  (define (average x y)
    (/ (+ x y) 2))
  (define (sqrt-iter guess next-guess)
    (define good-enough?
      (= guess next-guess)) ;; seems like cheating but works
    (if good-enough?
	guess
	(sqrt-iter next-guess
		   (improve next-guess))))
  (sqrt-iter 1.0 (improve 1.0)))
