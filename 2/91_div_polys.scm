(load "82_multi_coercions.scm")

(define (install-zero-package)
  ;; internal procedures
  (define (zero-rational? x)
    (define (numer x) (car x))
    (zero? (numer x)))
  (define (zero-complex? z)
    (= (real-part z) (imag-part z) 0))
  (define (zero-poly? p)
    (define (term-list p) (cdr p)) ;; selectors should be available for the whole system...
    (null? (term-list p))) 
  ;; interface for the rest of the system
  (put 'zero? '(scheme-number) zero?)
  (put 'zero? '(rational) zero-rational?)
  (put 'zero? '(complex) zero-complex?)
  (put 'zero? '(polynomial) zero-poly?)
  'done)

(install-zero-package)

(define (=zero? x) (apply-generic 'zero? x))


(define (install-sparse-poly-package)
  ;; internal procedures
  ;; representation of terms and term lists
  ;; As most polynomial manipulations are performed on
  ;; sparse polynomials, whose majority of coefficients is
  ;; zero, a term is a list containing an order of that term
  ;; and its coefficient. A term list is arranged from highest
  ;; to lowest-order term.
  (define (make-term order coeff) (list order coeff))
  (define (order term) (car term))
  (define (coeff term) (cadr term))
  
  (define (the-empty-termlist) '())
  (define (first-term term-list) (car term-list))
  (define (rest-terms term-list) (cdr term-list))
  (define (empty-termlist? term-list) (null? term-list))
  (define (adjoin-term term term-list)
    (if (=zero? (coeff term))
	term-list
	(cons term term-list)))

  ;; interface for the rest of the system
  (define (tag x) (attach-tag 'sparse x))
  (put 'first-term '(sparse) first-term)
  (put 'rest-terms '(sparse)
       (lambda (x) (tag (rest-terms x))))
  (put 'adjoin-term 'sparse
       (lambda (term term-list) (tag (adjoin-term term term-list))))
  (put 'empty-termlist? '(sparse) empty-termlist?)
  'done)

(install-sparse-poly-package)


(define (install-dense-poly-package)
  ;; internal procedures
  ;; representation of terms and term lists
  ;; A term is represented by its coefficient and place
  ;; in the term-list. Starting from the last term in the
  ;; list whose order is zero, the nth term in the term-list
  ;; has order n-1.
  (define (make-term order coeff) (list order coeff))
  (define (order term) (car term))
  (define (coeff term) (cadr term))

  (define (first-term term-list)
    (make-term (- (length term-list) 1) 
	       (car term-list)))
  (define (rest-terms term-list) (cdr term-list))
  (define (empty-termlist? term-list) (null? term-list))
  (define (adjoin-term term term-list)
    (cond ((=zero? (coeff term)) term-list)
	  ((= (order term) (length term-list))
	   (cons (coeff term) term-list))
	  (else (adjoin-term term (cons 0 term-list)))))

    ;; interface for the rest of the system
  (define (tag x) (attach-tag 'dense x))
  (put 'first-term '(dense) first-term)
  (put 'rest-terms '(dense)
       (lambda (x) (tag (rest-terms x))))
  (put 'adjoin-term 'dense
       (lambda (term term-list) (tag (adjoin-term term term-list))))
  (put 'empty-termlist? '(dense) empty-termlist?)
  'done)

(install-dense-poly-package)


(define (first-term term-list) (apply-generic 'first-term term-list))
(define (rest-terms term-list) (apply-generic 'rest-terms term-list))
(define (adjoin-term term term-list)
  ((get 'adjoin-term (type-tag term-list)) term (contents term-list)))
(define (empty-termlist? term-list)
  (apply-generic 'empty-termlist? term-list))
(define (the-empty-termlist) (attach-tag 'sparse '())) 
;; the empty-termlist brings some inconsistency to the polynomial system
;; because in the product of division of two polynomials the quotient
;; term-list can have different representation from the remainder

(define (install-polynomial-package)
  ;; internal procedures
  ;; representation of terms
  (define (make-term order coeff) (list order coeff))
  (define (order term) (car term))
  (define (coeff term) (cadr term))
  
  ;; representation of poly
  ;; Symbol [List-of Term] -> Poly
  (define (make-poly variable term-list) (cons variable term-list))
  (define (variable p) (car p))
  (define (term-list p) (cdr p))
  (define (variable? x) (symbol? x))
  (define (same-variable? x y) (eq? x y))

  ;; polynomial manipulations
  (define (add-poly p1 p2)
    (if (same-variable? (variable p1) (variable p2))
	(make-poly (variable p1)
		   (add-terms (term-list p1) (term-list p2)))
	(error "ADD-POLY" "Polys not in same var" (list p1 p2))))
  (define (add-terms L1 L2)
    (cond ((empty-termlist? L1) L2)
	  ((empty-termlist? L2) L1)
	  (else
	   (let ((t1 (first-term L1)) (t2 (first-term L2)))
	     (cond ((> (order t1) (order t2))
		    (adjoin-term
		     t1 (add-terms (rest-terms L1) L2)))
		   ((< (order t1) (order t2))
		    (adjoin-term
		     t2 (add-terms L1 (rest-terms L2))))
		   (else
		    (adjoin-term
		     (make-term (order t1)
				(add (coeff t1) (coeff t2)))
		     (add-terms (rest-terms L1)
				(rest-terms L2)))))))))

  (define (sub-poly p1 p2)
    (if (same-variable? (variable p1) (variable p2))
	(make-poly (variable p1)
		   (sub-terms (term-list p1) (term-list p2)))
	(error "SUB-POLY" "Polys not in same var" (list p1 p2))))
  (define (sub-terms L1 L2)
    (add-terms L1 (negate-terms L2)))
  (define (negate-terms term-list)
      (if (empty-termlist? term-list)
	  (the-empty-termlist)
	  (let ((first (first-term term-list)))
	    (adjoin-term (make-term (order first)
				    (negate (coeff first)))
			 (negate-terms (rest-terms term-list))))))
  
  (define (mul-poly p1 p2)
    (if (same-variable? (variable p1) (variable p2))
	(make-poly (variable p1)
		   (mul-terms (term-list p1) (term-list p2)))
	(error "MUL-POLY" "Polys not in same var" (list p1 p2))))
    (define (mul-terms L1 L2)
    (if (empty-termlist? L1)
	(the-empty-termlist)
	(add-terms (mul-term-by-all-terms (first-term L1) L2)
		   (mul-terms (rest-terms L1) L2))))
  (define (mul-term-by-all-terms t1 L)
    (if (empty-termlist? L)
	(the-empty-termlist)
	(let ((t2 (first-term L)))
	  (adjoin-term
	   (make-term (+ (order t1) (order t2))
		      (mul (coeff t1) (coeff t2)))
	   (mul-term-by-all-terms t1 (rest-terms L))))))

  (define (div-poly p1 p2)
    (if (same-variable? (variable p1) (variable p2))
	(make-poly (variable p1)
		   (div-terms (term-list p1) (term-list p2)))
	(error "MUL-POLY" "Polys not in same var" (list p1 p2))))

  (define (div-terms L1 L2)
    (if (empty-termlist? L1)
	(list (the-empty-termlist) (the-empty-termlist))
	(let ((t1 (first-term L1))
	      (t2 (first-term L2)))
	  (if (> (order t2) (order t1))
	      (list (the-empty-termlist) L1)
	      (let ((new-c (div (coeff t1) (coeff t2)))
		    (new-o (- (order t1) (order t2))))
		(let ((result-term (make-term new-o new-c)))
		  (let ((rest-of-result
			 (div-terms
			  (sub-terms L1 (mul-term-by-all-terms result-term L2))
			  L2)))
		    (cons (adjoin-term result-term (car rest-of-result))
			  (cdr rest-of-result))))))))) 
							     
  ;; interface for the rest of the system
  (define (tag p) (attach-tag 'polynomial p))
  (put 'add '(polynomial polynomial)
       (lambda (p1 p2) (tag (add-poly p1 p2))))
  (put 'sub '(polynomial polynomial)
       (lambda (p1 p2) (tag (sub-poly p1 p2))))
  (put 'mul '(polynomial polynomial)
       (lambda (p1 p2) (tag (mul-poly p1 p2))))
  (put 'div '(polynomial polynomial)
       (lambda (p1 p2) (tag (div-poly p1 p2))))
  (put 'make 'polynomial
       (lambda (var terms) (tag (make-poly var terms))))
  'done)

(install-polynomial-package)

(define (make-polynomial var terms)
  ((get 'make 'polynomial) var terms))

(define (install-negation-package)
  ;; internal-procedures
  (define (negate-rational x)
    (define (numer x) (car x))
    (define (denom x) (cdr x))
    (make-rational (- (numer x)) (denom x)))
  (define (negate-complex z)
    (make-complex-from-real-image (negate (real-part z))
				  (negate (imag-part z))))
  (define (negate-poly p)
    (define (variable p) (car p))
    (define (term-list p) (cdr p))
    (define (order t) (car t))
    (define (coeff t) (cadr t))
    (define (make-term order coeff)
      (list order coeff))
    (define (negate-terms term-list) ;; I could've saved space by installing it as a package
      (if (empty-termlist? term-list)
	  (the-empty-termlist)
	  (let ((first (first-term term-list)))
	    (adjoin-term (make-term (order first)
				    (negate (coeff first)))
			 (negate-terms (rest-terms term-list))))))
    (make-polynomial (variable p) (negate-terms (term-list p))))
    ;; interface for the rest of the system
  (put 'negate '(scheme-number) -)
  (put 'negate '(rational) negate-rational)
  (put 'negate '(complex) negate-complex)
  (put 'negate '(polynomial) negate-poly)
  'done)

(install-negation-package)

(define (negate x) (apply-generic 'negate x))


;; only (div p3 p4) gives incorrect results according to symbolab's long division
(define p1 (make-polynomial 'x (attach-tag 'dense '(1 0 0 0 0 -1))))
(define p2 (make-polynomial 'x (attach-tag 'sparse '((2 1) (0 -1)))))
(define p3 (make-polynomial 'x (attach-tag 'dense '(6 5 4 3 2 1))))
(define p4 (make-polynomial 'x (attach-tag 'sparse '((3 2) (1 1) (0 -1)))))
(define p5 (make-polynomial 'x (attach-tag 'dense '(1 -3 -10))))
(define p6 (make-polynomial 'x (attach-tag 'dense '(1 2))))
