;; [List-of Number] -> [List-of Number]
;; square every element in a list
(define (square-list items)
  (if (null? items)
      items
      (cons (square (car items))
	    (square-list (cdr items)))))

(define (square-list.v2 items)
  (map square items))

