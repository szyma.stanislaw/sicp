;; Let T be the time complexity of the procedure in ex. 2.42.
;; T in the previous exercise was proportional to the board
;; size k. In case of the below fragment time complexity would
;; be k^{k-1}T, where k is a specified board size, because
;; for each call of (queen-cols k) k new calls of
;; (queen-cols (- k 1)) will be generated, meaning that the
;; time complexity of this new procedure would be proportional to k^k
(flatmap
 (lambda (rest-of-queens)
   (map (lambda (new-row)
	  (adjoin-position new-row k rest-of-queens))
	(queen-cols (- k 1))))
 (enumerate-interval 1 board-size))
