(load "82_multi_coercions.scm")

;; Suppose that I will use Scheme's internal
;; representation of exact and inexact numbers
;; for my system's representation of real numbers.
(define (install-real-package)
  ;; internal procedures
  (define (make-real x) x)
  ;; interface for the rest of the system
  (define (tag x) (cons 'real x)) ;; I don't want to once again modify attach-tag...
  (put 'add '(real real)
       (lambda (x y) (tag (+ x y))))
  (put 'sub '(real real)
       (lambda (x y) (tag (- x y))))
  (put 'mul '(real real)
       (lambda (x y) (tag (* x y))))
  (put 'div '(real real)
       (lambda (x y) (tag (/ x y))))
  (put 'make 'real
       (lambda (x) (tag (make-real x))))
  'done) 

(install-real-package)

(define (make-real x) ((get 'make 'real) x))

;; At first, I did this and the next exercise using the
;; coercion table, but I think that in case of numeric
;; tower, which is straightforward relation, the "dirty"
;; way of doing it seems much easier. Coercion-table is
;; unfavorably flexible in this case.
(define (install-raise-package)
  ;; internal procedures
  (define (int->rat x) (make-rational x 1))
  (define (rat->real x)
    (define (numer x) (car x))
    (define (denom x) (cdr x))
    (make-real (/ (numer x) (denom x))))
  (define (real->complex x)
    (make-complex-from-real-imag x 0))
  ;; interface for the rest of the system
  (put 'raise 'scheme-number int->rat) 
  (put 'raise 'rational rat->real)
  (put 'raise 'real real->complex)
  'done)

(install-raise-package)

;; Object -> [Maybe Object]
;; Raises the object one level in a tower,
;; if it's impossible it returns #false
(define (raise x)
  (let ((candidate (get 'raise (type-tag x))))
    (if candidate
	(candidate (contents x))
	(error "RAISE"
	       "No method for this object"
	       x))))
