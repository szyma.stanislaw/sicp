;; [List-of X] -> [List-of X]
;; outputs a sequence in reverse order
(define (reverse seq)
  (define (iter a result)
    (if (null? a)
	result
	(iter (cdr a) (cons (car a) result))))
  (iter seq '()))

(define (reverse-foldl seq)
  (fold-left (lambda (x y) (cons y x))
	     '()
	     seq))

(define (reverse-foldr seq)
  (fold-right (lambda (x y) (append y (list x)))
	      '()
	      seq))
	
