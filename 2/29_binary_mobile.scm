;; Nobile is a structure
(define (make-mobile left right)
  (list left right)) 
;; Binary mobile consists of two branches (rods)
;; of certain length from which hangs either
;; a weight (number) or another binary mobile.

;; selectors
(define (left-branch m) (car m))
(define (right-branch m) (cadr m))

;; Branch is a structure
(define (make-branch length structure)
  (list length structure))
;; (make-branch Number Structure)

;; Structure is one of:
;; - Weigth (number)
;; - Mobile

;; selectors
(define (branch-length b) (car b))
(define (branch-structure b) (cadr b))

;; Mobile -> Number
(define (total-weight m)
  (cond ((null? m) 0)
	((number? m) m)
	(else
	 (+ (total-weight (branch-structure (left-branch m)))
	    (total-weight (branch-structure (right-branch m)))))))

;; Mobile -> Boolean
;; Is the torque on the left branch equal to that in
;; the right one (including submobiles). Torque is a
;; product of length of a branch and the weight
;; hanging from it.
(define (balanced? m)
  (define (torque branch)
    (* (branch-length branch)
       (total-weight (branch-structure branch))))
  (if (number? m)
      #t
      (and (= (torque (left-branch m)) (torque (right-branch m)))
	   (balanced? (branch-structure (left-branch m)))
	   (balanced? (branch-structure (right-branch m))))))

(define x (make-mobile
	   (make-branch 3 (make-mobile
			   (make-branch 5 6)
			   (make-branch 5 6)))
	   (make-branch 3 (make-mobile
			   (make-branch 5 6)
			   (make-branch 5 6)))))

(define y (make-mobile
	   (make-branch 3 (make-mobile
			   (make-branch 5 6)
			   (make-branch 5 6)))
	   (make-branch 3 (make-mobile
			   (make-branch 5 6)
			   (make-branch 5 5))))) ;; minor change

(newline)
(display (balanced? x))
(newline)
(display (balanced? y))

;; d. No matter if the representation of binary mobiles is
;; based on lists or pairs the above functions should work.
;; We only change the form in which the data is stored,
;; not the data we store, so after changing the selectors
;; total-weight and balanced? procedure should work.
