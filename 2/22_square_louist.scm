;; In this procedure each consecutive element will be
;; "consed" into the list in accumulator after the
;; preceding one, meaning that the resulting list will
;; be in reversed order.
(define (square-list.v1 items)
  (define (iter things answer)
    (if (null? things)
	answer
	(iter (cdr things)
	      (cons (square (car things))
		    answer)))))
(iter items '()))

;; In this procedure the list in the accumulator is
;; "consed" with some squared number, but it is not
;; equivalent of adding a new element at the end
;; of the list, but creating a pair of that list
;; and that element
(define (square-list.v2 items)
  (define (iter things answer)
    (if (null? things)
	answer
	(iter (cdr things)
	      (cons answer
		    (square (car things))))))
(iter items '()))
