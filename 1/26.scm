;; The following procedure has to make
;; the same computation twice meaning
;; that it is a tree-recursive process
;; with exponential order of growth
;; turning initially O(logn) process
;; into a O(n) process.

(define (expmod base exp m)
  (cond ((= exp 0) 1)
	((even? exp)
	 (remainder (* (expmod base (/ exp 2) m)
		       (expmod base (/ exp 2) m))
		    m))
	(else
	 (remainder (* base (expmod base (- exp 1) m))
		    m))))
