;; N N -> [List-of [List N N N]]
;; produces a sequence of all ordered triples of
;; distinct positive integers i,j,k less than or
;; equal to a given integer n that sum to a given
;; integer s
(define (triples n s)
  (define (flatmap proc seq)
    (fold-right append '() (map proc seq)))
  (define (enumerate-interval low high)
    (if (> low high)
	'()
	(cons low (enumerate-interval (+ low 1) high))))
  (define (equal-sum? triple)
    (= s (+ (car triple) (cadr triple) (caddr triple))))
  (filter equal-sum?
	  (flatmap (lambda (i)
		     (flatmap (lambda (j)
				(map (lambda (k) (list i j k))
				     (enumerate-interval 1 (- j 1))))
			      (enumerate-interval 1 (- i 1))))
		   (enumerate-interval 1 n))))
