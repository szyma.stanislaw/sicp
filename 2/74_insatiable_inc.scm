;; From section 3.3.3 for section 2.4.3
;; to support operation/type table for data-directed dispatch

(define (assoc key records)
  (cond ((null? records) false)
        ((equal? key (caar records)) (car records))
        (else (assoc key (cdr records)))))

(define (make-table)
  (let ((local-table (list '*table*)))
    (define (lookup key-1 key-2)
      (let ((subtable (assoc key-1 (cdr local-table))))
        (if subtable
            (let ((record (assoc key-2 (cdr subtable))))
              (if record
                  (cdr record)
                  false))
            false)))
    (define (insert! key-1 key-2 value)
      (let ((subtable (assoc key-1 (cdr local-table))))
        (if subtable
            (let ((record (assoc key-2 (cdr subtable))))
              (if record
                  (set-cdr! record value)
                  (set-cdr! subtable
                            (cons (cons key-2 value)
                                  (cdr subtable)))))
            (set-cdr! local-table
                      (cons (list key-1
                                  (cons key-2 value))
                            (cdr local-table)))))
      'ok)    
    (define (dispatch m)
      (cond ((eq? m 'lookup-proc) lookup)
            ((eq? m 'insert-proc!) insert!)
            (else (error "Unknown operation -- TABLE" m))))
    dispatch))

(define operation-table (make-table))
(define get (operation-table 'lookup-proc))
(define put (operation-table 'insert-proc!))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; a) Each division's files should not only supply data
;; about the employees, but also an information on how to
;; process it, i.e. a package of selectors allowing to find
;; employee's salary and address, and a general procedure
;; that looks up the record of a specified employee. Thus each
;; division's record should have a type tag with the name of
;; the division, e.g. 'division-x to allow the generic
;; procedures to discern which selectors should be applied.

(define (tag file) (car file))
(define (content file) (cdr file))

;; File String -> Record
;; Obtain a record of a given employee
(define (get-record file name)
  ((get 'get-employee (tag file)) (content file) name))


;; b)

;; File String -> Number
(define (get-salary file name)
  ((get 'get-employee-salary (tag file)) (get-record file name)))


;; c)

;; [List-of File] String -> Record
(define (find-employee-record files name)
  (if (null? files)
      (error "No employee in the system -- FIND-EMPLOYEE-RECORD" name)
      (let ((candidate-record (get-record (car files) name)))
	(if candidate-record
	    candidate-record
	    (find-employee-record (cdr files) name)))))

;; d) If a new division is created, a new package of
;; selectors also must be created.
	      
