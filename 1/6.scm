(define (new-if predicate then-clause else-clause)
  (cond (predicate then-clause)
	(else else-clause)))

(define (sqrt-iter guess x)
  (define (good-enough? guess x)
    (< (abs (- (square guess) x)) 0.001))
  (define (improve guess x)
    (average guess (/ x guess)))
  (define (average x y)
    (/ (+ x y) 2))
  (new-if (good-enough? guess x)
	  guess
	  (sqrt-iter (improve guess x) x)))

;; In case when the applicative-order interpreter is used
;; sqrt-iter will loop forever, because the operands of
;; new-if procedure application are evaluated before the
;; evaluation of the procedure call. Using normal-order
;; evaluation square root would be computed as usual.
