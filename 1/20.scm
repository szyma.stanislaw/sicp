;; computes (a,b) given that
;; (a,b) = (b, a-b) = (b, a(mod b))
(define (gcd a b)
  (if (= b 0)
      a
      (gcd b (remainder a b))))



;; substitution model in case of normal-order evaluation:
(gcd 206 40)
(gcd 40 (remainder 206 40))
;; [if-clause] is evaluated
(= (remainder 206 40) 0) ;; here remainder is evaluated once
(= 6 0)
#f
;; [else-clause] is evaluated
(gcd (remainder 206 40) (remainder 40 (remainder 206 40)))
;; [if-clause] is evaluated
(= (remainder 40 (remainder 206 40)) 0) ;; here twice
(= (remainder 40 6) 0)
(= 4 0)
#f
;; [else-clause] is evaluated
(gcd (remainder 40 (remainder 206 40))
     (remainder (remainder 206 40)
		(remainder 40 (remainder 206 40))))
;; [if-clause] is evaluated
(= (remainder (remainder 206 40)  ;; here four times
	      (remainder 40 (remainder 206 40)))
   0)
(= (remainder 6 (remainder 40 6)) 0)
(= (remainder 6 4) 0)
(= 2 0)
#f
;; [else-clause] is evaluated
(gcd (remainder (remainder 206 40)
		(remainder 40 (remainder 206 40)))
     (remainder (remainder 40 (remainder 206 40))
		(remainder (remainder 206 40)
			   (remainder 40 (remainder 206 40))))) ;; fuck
;; [if-clause] is evaluated
(= (remainder (remainder 40 (remainder 206 40))
	      (remainder (remainder 206 40)
			 (remainder 40 (remainder 206 40))))  ;; seven times
   0)
(= (remainder (remainder 40 6)
	      (remainder 6 (remainder 40 6)))
   0)
(= (remainder 4 (remainder 6 4)) 0)
(= (remainder 4 2) 0)
(= 0 0)
#t
;; finally [then-clause] is evaluated
(remainder (remainder 206 40)
	   (remainder 40 (remainder 206 40))) ;; four times
(remainder 6 (remainder 40 6))
(remainder 6 4)
2
;; in total remainder procedure is evaluated 18 times



;; in case of applicative-order evaluation
(gcd 206 40)
(gcd 40 (remainder 206 40)) ;; once
(gcd 40 6)
(gcd 6 (remainder 40 6)) ;; once
(gcd 6 4)
(gcd 4 (remainder 6 4)) ;; once
(gcd 4 2)
(gcd 2 (remainder 4 2)) ;; once
(gcd 2 0)
2
;; here remainder procedure is evaluated 4 times
