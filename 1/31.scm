;; [Number -> Number] Number [Number -> Number] Number
;; -> Number
;; returns the product of values of a function at points
;; over a given range
(define (product term a next b)
  (if (> a b)
      1
      (* (term a)
	 (product term (next a) next b))))

;; factorial written using product abstraction
(define (factorial n)
  (define (inc x) (+ x 1))
  (define (identity x) x)
  (product identity 1 inc n))

;; approximates pi/4
(define (pi-product n)
  (define (term x)
    (if (even? x)
	(/ (+ x 2) (+ x 1))    ;; I love the fact that you can sometimes
	(/ (+ x 1) (+ x 2))))  ;; solve a problem in such an unusual way
  (define (inc x) (+ x 1))
  (product term 1.0 inc n))
  

;; iterative version of a product procedure
(define (product-iter term a next b)
  (define (iter a result)
    (if (> a b)
	result
	(iter (next a) (* result (term a)))))
  (iter a 1))

