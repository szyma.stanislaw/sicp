;; [N -> N] [N -> N] -> [N -> N]
;; composes f after g or f(g(x))
(define (compose f g)
  (lambda (x) (f (g x))))

(newline)
(display ((compose square inc) 6))
(newline)

(define (inc x) (+ x 1))



	 
