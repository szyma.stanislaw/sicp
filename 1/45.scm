;; computes n-th root of 
(define (root n x)
  (define (map y) (/ x (expt y (- n 1))))
  (fixed-point ((repeated average-damp 3) map) ;; three average damps suffice
	       1.0))


;; [N -> N] N -> [N -> N]
;; given procedure f produces
;; f(f(...(f(f(x)))...)) n times,
;; where n is a positive integer
(define (repeated f n)
  (define (compose f g)
    (lambda (x) (f (g x))))
  (if (= n 1)
      f
      (compose f (repeated f (- n 1)))))

(define (average-damp f)
  (define (average a b) (/ (+ a b) 2))
  (lambda (x) (average x (f x))))

;; finds the root of eq. x = f(x)
(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
	  next
	  (try next))))
  (try first-guess))

(define tolerance 0.00001)
