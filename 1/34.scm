(define (f g)
  (g 2))

;; combination (f f) would yield an error
;; since f is a procedure that accepts
;; other procedures that are applicable to
;; 2 as its arguments:
;; (f f)
;; (f 2)
;; (2 2)
