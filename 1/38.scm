;; Approximation of a continued fraction expansion for e-2
;; due to Leonhard Euler.
(define e (+ (cont-frac (lambda (i) 1.0)
			(lambda (i) (if (= (remainder i 3) 2)
					(+ (* (/ 2.0 3.0)
					      (- i 2))
					   2)
					1))
			10)
	     2))

;;  I derived second lambda based on the fact that in series
;;             1, 2, 1, 1, 4, 1, 1, 6, 1, 1, 8, ...
;; all elements a_i where i is 2 greater than any multiple of 3
;; are different from 1. Therefore any element a_i where i
;; is not congruent to 2 modulo 3 will be equal to 1. Now
;; we can focus on the second conditional which determines
;; the value of an element a_i where i is congruent 2 (mod 3).
;; We simplify the series by numerating every element which
;; satisfies the above and by omitting everything else.

;; table for visualisation
;; a_i | 2 | 4 | 6 | 8  | ...
;; i   | 2 | 5 | 8 | 11 | ...

;; We can observe that a_i grows by 2 with every new element
;; while i grows by 3. Hence, given i we obtain following eq.
;; a_i = (n-2)/3 * 2 + 2
;;     = 2/3(n-2) + 2


;; [X -> Number] [X -> Number] Number -> Number
;; approximates an infinite continued fraction
;; until kth term
(define (cont-frac n d k)
  (define (continue count)
    (if (> count k)
	0
	(/ (n count)
	   (+ (d count)
	      (continue (+ count 1))))))
  (continue 1))
