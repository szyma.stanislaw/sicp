(define (p) (p))

(define (test x y)
  (if (= x 0)
      0
      y))

;; the procedure application (test 0 (p)) will not evaluate when
;; using the aplicative-order interpreter, because (p) which is
;; defined as itself has to be evaluated first. When using the
;; normal-order interpreter the function call would be evaluated
;; as usual.
