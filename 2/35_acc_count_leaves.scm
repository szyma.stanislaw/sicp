;; Tree -> Number
(define (count-leaves t)
  (accumulate + 0
	      (map (lambda (subtree) (if (pair? subtree)
					 (count-leaves subtree)
					 1))
		   t)))

(define x '(1 2 (3 5) (3 9 8 (34 8)) (3 8 (35))))

(newline)
(display (count-leaves x)) ;; should be 12
