;; Now set is an unordered list which can have duplicates.
;; Hence {1, 2, 3} can be represented as (2 3 2 1 3 2 2).

(define (element-of-set? x set) ;; T = O(n)
  (cond ((null? set) #f)
	((equal? x (car set)) #t)
	(else (element-of-set? x (cdr set)))))

(define (adjoin-set x set) (cons x set)) ;; T = O(1)

(define (union-set set1 set2) (append set1 set2)) ;; T = O(n)

(define (intersection-set set1 set2) ;; T = O(n^2)
  (cond ((or (null? set1) (null? set2)) '())
	((element-of-set? (car set1) set2)
	 (cons (car set1)
	       (intersection-set (cdr set1) set2)))
	(else (intersection-set (cdr set1) set2))))

;; Since duplicates are allowed, in the best case element-of-set?
;; will be more efficient, and in the worst scenario as efficient
;; as the prior representation.
