;; Rational is a pair of integers, where
;; n denotes numerator and d denominator.
(define (make-rat n d) ;; the constructor 
  (define (gcd a b)
    (if (= b 0)
	a
	(gcd b (remainder a b))))
  (let ((g (gcd (abs n) (abs d))))
    (cons ((if (positive? d)
	       +
	       -) (/ n g)) 
	  (/ (abs d) g))))

;; selectors
(define (numer x) (car x))
(define (denom x) (cdr x))

;; examples
(define one-half (make-rat 1 2))
(define one-third (make-rat 1 3))
(define one (make-rat -4 -4))
(define minustwo (make-rat 4 -2))

(define (print-rat x)
  (newline)
  (display (numer x))
  (display "/")
  (display (denom x)))

;; arithmetic on rationals
(define (add-rat x y)
  (make-rat (+ (* (numer x) (denom y))
	       (* (numer y) (denom x)))
	    (* (denom x) (denom y))))

(define (sub-rat x y)
  (make-rat (- (* (numer x) (denom y))
	       (* (numer y) (denom x)))
	    (* (denom x) (denom y))))

(define (mul-rat x y)
  (make-rat (* (numer x) (numer y))
	    (* (denom x) (denom y))))

(define (div-rat x y)
  (make-rat (* (numer x) (denom y))
	    (* (numer y) (denom x))))


