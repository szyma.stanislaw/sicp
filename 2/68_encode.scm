(define (make-leaf symbol weight)
  (list 'leaf symbol weight))

(define (leaf? object)
  (eq? (car object) 'leaf))

(define (symbol-leaf x) (cadr x))
(define (weight-leaf x) (caddr x))


(define (make-code-tree left right)
  (list left
	right
	(append (symbols left) (symbols right))
	(+ (weight left) (weight right))))

(define (left-branch tree) (car tree))
(define (right-branch tree) (cadr tree))

(define (symbols tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))

(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))

(define sample-tree
  (make-code-tree (make-leaf 'A 4)
		  (make-code-tree
		   (make-leaf 'B 2)
		   (make-code-tree (make-leaf 'D 1)
				   (make-leaf 'C 1)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; [List-of Symbol] Tree -> [Maybe Bits]
(define (encode message tree)
  (if (null? message)
      '()
      (append (encode-symbol (car message) tree)
	      (encode (cdr message) tree))))

;; Symbol Tree -> [Maybe Bits]
(define (encode-symbol s t)
  (if (leaf? t)
      '()
      (let ((left (left-branch t))
	    (right (right-branch t)))
	(cond ((member? s (symbols left))
	       (cons 0
		     (encode-symbol s left)))
	      ((member? s (symbols right))
	       (cons 1
		     (encode-symbol s right)))
	      (else
	       (error "no symbol in tree -- ENCODE-SYMBOL" symbol))))))

(define (member? symbol list)
  (cond ((null? list) #f)
	((eq? (car list) symbol) #t)
	(else (member? symbol (cdr list))))) 


(newline)
(display (encode '(a d a b b c a) sample-tree))
