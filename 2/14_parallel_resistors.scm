;; Interval is a structure
(define (make-interval a b) (cons a b))
;; where a and b represent the lower and
;; the upper bound of an interval

;; selectors
(define (lower-bound i) (car i))
(define (upper-bound i) (cdr i))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
		 (+ (upper-bound x) (upper-bound y))))

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
	(p2 (* (lower-bound x) (upper-bound y)))
	(p3 (* (upper-bound x) (lower-bound y)))
	(p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
		   (max p1 p2 p3 p4))))

(define (div-interval x y)
  (let ((lowy (lower-bound y))
	(upy (upper-bound y)))
    (if (or (= lowy 0) (= upy 0))
	(error "div-internal: division by zero")
	(mul-interval x (make-interval (/ 1.0 upy)
				       (/ 1.0 lowy))))))

(define (sub-interval x y)
  (make-interval (- (lower-bound x) (upper-bound y))
		 (- (upper-bound x) (lower-bound y))))

;; Center/Percent is a structure
(define (make-center-percent c p)  ;; this is an impercise procedure
  (make-interval (* c (- 1.0 (/ p 100.0))) 
		 (* c (+ 1.0 (/ p 100.0)))))

;; selectors
(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))

(define (percent i)
  (* (/ (- (upper-bound i) (lower-bound i))
	(+ (upper-bound i) (lower-bound i)))
     100))

(define (mul-center-percent x y)
  (make-center-percent (* (center x) (center y))
		       (+ (percent x) (percent y))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (par1 r1 r2)
  (div-interval (mul-interval r1 r2)
		(add-interval r1 r2)))

(define (par2 r1 r2)
  (let ((one (make-interval 1 1)))
    (div-interval one
		  (add-interval (div-interval one r1)
				(div-interval one r2)))))

(define A (make-center-percent 15.0 30))
(define B (make-center-percent 8. .6))

;; par2 has more accurate results
(newline)
(display (center (par1 A B)))
(newline)
(display (percent (par1 A B)))
(newline)
(newline)
(display (center (par2 A B)))
(newline)
(display (percent (par2 A B)))
(newline)

;;A/A
(newline)
(display (div-interval (make-center-percent 15. 30.)
		       (make-center-percent 15. 30.)))
(newline)
;; those two intervals are equal yet the division yields
;; an interval with the smallest lower bound and the biggest
;; upper bound. This interval package lacks the concept of
;; identity.


;; A - A
(newline)
(display (sub-interval A A))
(newline)

;; distributive property, which works
(newline)
(display (mul-interval A (add-interval A B)))
(newline)
(display (add-interval (mul-interval A A)
		       (mul-interval A B)))
(newline)
