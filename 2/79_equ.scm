(load "77-78_arithmetic_system.scm")

;; The issue is that 4 = 4 + 0 * i
;; Let's assume that the numbers of
;; two different types are never equal (I would like to know how).

(define (install-equality-package)
  ;; internal procedures
  (define (=rational? x y)
    (define (numer x) (car x))
    (define (denom x) (cdr x))
    (= (* (numer x) (denom y)) ;; numer and denom are relatively prime tough
       (* (denom x) (numer y)))) 
  (define (=complex? x y)
    (and (= (real-part x) (real-part y))
	 (= (imag-part x) (imag-part y))))
  ;; interface for the rest of the system
  (put 'equal? '(scheme-number scheme-number) =)
  (put 'equal? '(rational rational) =rational?)
  (put 'equal? '(complex complex) =complex?)
  'done)

(install-equality-package)

(define (equ? x y)
  (if (eq? (type-tag x) (type-tag y))
      (apply-generic 'equal? x y)
      #f))

