;; Number N -> Number
;; iteratively computes b^n

(define (expt b n0)
  ;; N -> Boolean
  (define (even? n)
    (= (remainder n 2) 0))
  ;; accumulator a stores b^{n0 - n} 
  (define (iter b n a)
    (cond ((= n 0) a)
	  ((even? n) (iter (square b) (/ n 2) a)) 
	  (else (iter b (- n 1) (* a b)))))
  (iter b n 1))

