;; Point is a structure
(define (make-point x y) (cons x y))
;; (make-point Number Number)
;; x and y are real numbers that represent
;; x and y-coordinate of a point

;; examples
(define A (make-point 1.0 1.0))
(define B (make-point 1.0 5.0))
(define C (make-point 16.0 5.0))
(define D (make-point 16.0 1.0))

;; selectors
(define (x-point p) (car p))
(define (y-point p) (cdr p))


;; Segment is a structure
(define (make-segment p q) (cons p q))
;; (make-segment Point Point)
;; p and q denote two points that represent
;; line segments in a plane

;; example
(define AB (make-segment A B))
(define BC (make-segment B C))
(define CD (make-segment C D))
(define DA (make-segment D A))

;; selectors
(define (start-segment s) (car s))
(define (end-segment s) (cdr s))

;;;;;;;;;;; First representation ;;;;;;;;;;;;;;

;; Rectangle.v1 is a structure
(define (make-rec.v1 a b) (cons a b))
;; (make-rec Segment Segment)
;; interpretation: a and b represent
;; two adjacent sides of a rectangle
;; in a plane

;; example
(define rec.v1 (make-rec.v1 AB BC))

;; selectors
(define (length r) (car r))
(define (width r) (cdr r))

;; Segment -> Number
(define (distance s)
  (let ((A (start-segment s))
	(B (end-segment s)))
    (sqrt (+ (square (- (x-point A)
			(x-point B)))
	     (square (- (y-point A)
			(y-point B)))))))


;;;;;;;;;; Second Representation ;;;;;;;;;;;;

;; Rectangle is a structure
(define (make-rec.v2 P x y) (cons P (cons x y)))
;; (make-rec Point Number Number)
;; interpretation: let P represent
;; a vertex of a rectangle with coordinates
;; (x_P, y_P), then the other three vertices
;; have coordinates:
;; - (x_P + x, y_P),
;; - (x_P, y_P + y),
;; - (x_P + x, y_P + y)

;; examples
(define Q (make-point 1.0 5.0))
(define rec.v2 (make-rec.v2 Q -4 5))

;; selectors
(define (vertex r) (car r))
(define (horizontal-shift r) (car (cdr r)))
(define (vertical-shift r) (cdr (cdr r)))

;; if you change names of the procedures
;; length and width into horizontal-shift
;; and vertical-shift respectively, and
;; distance.v1 into abs it
;; should work the same.

;; Rectangle -> Number
(define (perimeter r)
  (* (+ (distance (length r))
	(distance (width r)))
     2))

;; Rectangle -> Number
(define (area r)
  (* (distance (length r))
     (distance (width r))))


