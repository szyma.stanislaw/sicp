;; [List-of X] -> [List-of X]
;; reverses every sublist and the list itself
(define (deep-reverse items)
  (cond ((null? items) items)
	((not (pair? items)) items)
	(else
	 (reverse
	  (cons (deep-reverse (car items))
		(deep-reverse (cdr items)))))))

(define x (list (list 1 2) (list 3 4)))
(newline)
(display (reverse x))
(newline)
(display (deep-reverse x))
