;; From section 3.3.3 for section 2.4.3
;; to support operation/type table for data-directed dispatch

(define (assoc key records)
  (cond ((null? records) #f)
        ((equal? key (caar records)) (car records))
        (else (assoc key (cdr records)))))

(define (make-table)
  (let ((local-table (list '*table*)))
    (define (lookup key-1 key-2)
      (let ((subtable (assoc key-1 (cdr local-table))))
        (if subtable
            (let ((record (assoc key-2 (cdr subtable))))
              (if record
                  (cdr record)
                  #f))
            #f)))
    (define (insert! key-1 key-2 value)
      (let ((subtable (assoc key-1 (cdr local-table))))
        (if subtable
            (let ((record (assoc key-2 (cdr subtable))))
              (if record
                  (set-cdr! record value)
                  (set-cdr! subtable
                            (cons (cons key-2 value)
                                  (cdr subtable)))))
            (set-cdr! local-table
                      (cons (list key-1
                                  (cons key-2 value))
                            (cdr local-table)))))
      'ok)    
    (define (dispatch m)
      (cond ((eq? m 'lookup-proc) lookup)
            ((eq? m 'insert-proc!) insert!)
            (else (error "Unknown operation -- TABLE" m))))
    dispatch))

(define operation-table (make-table))
(define get (operation-table 'lookup-proc))
(define put (operation-table 'insert-proc!))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Expression is one of:
;; - Number
;; - Variable, which a symbol such as 'x
;; - (List Operator Expression Expression), where Operator is a symbol such as '+ 

(define (deriv exp var)
  (cond ((number? exp) 0)
	((variable? exp) (if (same-variable? exp var) 1 0))
	(else
	 ((get 'deriv (operator exp)) (operands exp) var))))

(define (variable? exp) (symbol? exp))

(define (same-variable? exp var) (eq? exp var))

(define (operator exp) (car exp))

(define (operands exp) (cdr exp))


;; a)
;; This generic version of a program performing symbolic differentation
;; uses get procedure to look up in a table a differentiation rule for
;; a given operator and applies it to the operands. Because Numbers and
;; Variables are plain symbols, there is no type that can be dispatched.


;; b)

;; Expression Number -> Boolean
(define (=number? x n)
  (and (number? x) (= x n)))

;; Expression Expression -> Expression
(define (make-sum a1 a2)
  (cond ((=number? a1 0) a2)
	((=number? a2 0) a1)
	((and (number? a1) (number? a2)) (+ a1 a2))
	(else (list '+ a1 a2))))

;; Expression Expression -> Expression 
(define (make-product m1 m2)
  (cond ((or (=number? m1 0) (=number? m2 0)) 0)
	((=number? m1 1) m2)
	((=number? m2 1) m1)
	((and (number? m1) (number? m2)) (* m1 m2))
	(else (list '* m1 m2))))

(define (install-deriv-base-package)
  ;; internal procedures
  (define (addend loi) (car loi))
  (define (augend loi) (cadr loi))
  (define (multiplier loi) (car loi))
  (define (multiplicand loi) (cadr loi))
  (define (deriv-sum loi var)
    (make-sum (deriv (addend loi) var)
	      (deriv (augend loi) var)))
  (define (deriv-product loi var)
    (make-sum (make-product (multiplier loi)
			    (deriv (multiplicand loi) var))
	      (make-product (deriv (multiplier loi) var)
			    (multiplicand loi))))
  ;; interface for the rest of the system
  (put 'deriv '+ deriv-sum)
  (put 'deriv '* deriv-product)
  'done)

(install-deriv-base-package)

;; c)

;; Expression Number -> Expression
(define (make-expt base power)
  (cond ((=number? power 0) 1)
	((or (=number? base 0) (=number? base 1)) base)
	((number? base) (expt base power))
	(else (list '** base power))))


(define (install-deriv-exponentiation-package)
  ;; internal procedures
  (define (base loi) (car loi))
  (define (power loi) (cadr loi))
  (define (deriv-expt loi var)
    (make-product (power loi)
		  (make-product (make-expt (base loi) (- (power loi) 1))
				(deriv (base loi) var))))
  ;; interface for the rest of the system
  (put 'deriv '** deriv-expt)
  'done)

(install-deriv-exponentiation-package)

;; d)
;; If we change the last fragment of the deriv procedure definiton to:
;; ((get (operator exp) 'deriv) (operands exp) var)
;; the only change required to the derivative system is:
;; (put <operator> 'deriv <procedure>)
;; instead of:
;; (put 'deriv <operator> <procedure>)
