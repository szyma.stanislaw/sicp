;; multiplication in modular arithmetic says that
;; for integers a, b, c, d and positive integer m
;; ab is congruent to cd(mod m);
;; meaning that the expmod procedure below allows
;; us to deal with much smaller numbers while first
;; computing a(mod m) and b(mod m), and then their
;; product modulo m.

;; expmod-hacker procedure however does everything
;; at once which means that for very large numbers
;; arithmetic computations will become costly.

(define (expmod base exp m)
  (cond ((= b 0) 1)
	((even? exp)
	 (remainder (square (expmod base (/ exp 2) m))
		    m))
	(else (remainder (* base (expmod base (- exp 1) m))
			 m))))


(define (expmod-hacker base exp m)
  (remainder (fast-expt base exp) m))

(define (fast-exp a b)
  (cond ((= b 0) 1)
	((even? b)
	 (square (fast-exp a (/ b 2))))
	(else (* a (fast-exp (- b 1))))))

