;; N N -> N
;; represents a pair of nonnegative
;; integers a and b as a product of
;; 2^a * 3^b
(define (cons a b) (* (expt 2 a) (expt 3 b)))

(define (car z)
  (define (iter z a)
    (if (not (= (remainder z 2) 0))
	a
	(iter (/ z 2) (+ a 1))))
  (iter z 0))

(define (cdr z)
  (define (iter z b)
    (if (not (= (remainder z 3) 0))
	b
	(iter (/ z 3) (+ b 1))))
  (iter z 0))
