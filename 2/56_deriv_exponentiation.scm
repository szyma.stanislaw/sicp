;; Variable is a Symbol
;; Operator is a Symbol

;; Expression is one of:
;; - Number
;; - Variable
;; - (List Operator Expression Expression)

;; Expression -> Boolean
;; is x a variable?
(define (variable? x) (symbol? x))

;; Expression Expression -> Boolean
(define (same-variable? v1 v2)
  (and (variable? v1) (variable? v2) (eq? v1 v2)))

;; Expression Number -> Boolean
(define (=number? exp num)
  (and (number? exp) (= exp num)))

;; Expression Expression -> Expression
(define (make-sum a1 a2)
  (cond ((=number? a1 0) a2)
	((=number? a2 0) a1)
	((and (number? a1) (number? a2)) (+ a1 a2))
	(else (list '+ a1 a2))))

(define (make-product m1 m2)
  (cond ((or (=number? m1 0) (=number? m2 0)) 0)
	((=number? m1 1) m2)
	((=number? m2 1) m1)
	((and (number? m1) (number? m2)) (* m1 m2))
	(else (list '* m1 m2))))

;; Expression Number -> Expression
(define (make-exponentiation base exponent)
  (cond ((=number? exponent 0) 1)
	((=number? exponenent 1) base)
	((number? base) (exp base exponent))
	(else (list '** base exponent))))

;; Expression -> Boolean
(define (sum? x)
  (and (pair? x) (eq? (car x) '+)))

(define (product? x)
  (and (pair? x) (eq? (car x) '*)))

(define (exponentiation? exp)
  (and (pair? exp) (eq? (car exp) '**)))

;; Expression -> Expression
(define (addend s) (cadr s))
(define (augend s) (caddr s))

(define (multiplier p) (cadr p))
(define (multiplicand p) (caddr p))

(define (base e) (cadr e))
(define (exponent e) (caddr e))

;; Expression Variable -> Expression
(define (deriv exp var)
  (cond ((number? exp) 0)
	((variable? exp)
	 (if (same-variable? exp var) 1 0))
	((sum? exp)
	 (make-sum (deriv (addend exp) var)
		   (deriv (augend exp) var)))
	((product? exp)
	 (make-sum
	  (make-product (multiplier exp)
			(deriv (multiplicand exp) var))
	  (make-product (deriv (multiplier exp) var)
			(multiplicand exp))))
	((exponentiation? exp)
	 (let ((base (base exp))
	       (exponent (exponent exp)))
	   (make-product
	    (make-product exponent
			  (make-exponentiation base (- exponent 1)))
	    (deriv base var))))
	(else
	 (error "unknown expression type -- DERIV" exp))))

(newline)
(display (deriv '(+ (** (+ x 5) 0) (* x 5)) 'x))
(newline)
(display (deriv '(** (+ (* x 5) (+ y 3)) 7) 'x))
(newline)
(display (deriv '(+ (** (+ x -1) 5) (* x y)) 'x))
