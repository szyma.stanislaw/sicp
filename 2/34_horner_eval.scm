;; Number [List-of Number]
;; Evaluates a polynomial in x at a given value of x.
;; Assume that the sequence of coefficients is arranged
;; from a_0 through a_n
(define (horner-eval x coefficient-sequence)
  (accumulate (lambda (this-coefficient higher-terms)
		(+ this-coefficient
		   (* x higher-terms)))
	      0
	      coefficient-sequence))
