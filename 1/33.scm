;; [X X -> Y] [X -> Boolean] X [X -> X] X [X -> X] X
;; -> Y
(define (filtered-accumulate combiner filter null-value term a next b)
  (define (iter a result)
    (if (> a b)
	result
	(iter (next a) (combiner result (if (filter a)
					    (term a)
					    null-value)))))
  (iter a null-value))

;; N N -> N
;; computes the sum of squares of primes in [a,b] interval
(define (sum-sq-prime a b) 
  (define (prime? n)
    (if (= n 1)
	#f
	(= n (smallest-divisor n))))
  (define (smallest-divisor n)
    (find-divisor n 2))
  (define (find-divisor n test-divisor)
    (cond ((> (square test-divisor) n) n)
	  ((divides? test-divisor n) test-divisor)
	  (else (find-divisor n (+ test-divisor 1)))))
  (define (divides? a b)
    (= (remainder b a) 0))
  (define (inc x) (+ x 1))
  (filtered-accumulate + prime? 0 square a inc b))

;; N -> N
;; computes the product of all positive integers less than n
;; that are relatively prime to n
(define (product-rel-prime n)
  (define (gcd a b)
    (if (= b 0)
	a
	(gcd b (remainder a b))))
  (define (rel-prime? x)
    (= (gcd n x) 1))
  (define (identity x) x)
  (define (inc x) (+ x 1))
  (filtered-accumulate * rel-prime? 1 identity 2 inc (- n 1)))  


;; looks like both work
(newline)
(display (= (sum-sq-prime 1 20)
	    (+ (square 2) (square 3)
	       (square 5) (square 7)
	       (square 11) (square 13)
	       (square 17) (square 19))))
(newline)

(display (= (product-rel-prime 10)
	    (* 3 7 9)))
(newline)
