;; If b is positive it is added to a, otherwise substracted from.
;; In other words - the absolute value of b is added to a
(define (a-plus-abs-b a b)
  ((if (> b 0) + -) a b))
