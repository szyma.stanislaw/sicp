;; [List-of X] -> [List-of X]
;; returs a list in reverse order
(define (reverse list)
  (define (iter a result)
    (if (null? a)
	result
	(iter (cdr a) (cons (car a) result))))
  (iter list '())) ;; I don't know why nil doesn't work

(newline)
(display (reverse (list 1 4 9 16 25)))
