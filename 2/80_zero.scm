(load "79_equ.scm")

(define (install-zero-package)
  ;; internal procedures
  (define (zero-rational? x)
    (define (numer x) (car x))
    (zero? (numer x)))
  (define (zero-complex? z)
    (= (real-part z) (imag-part z) 0))
  ;; interface for the rest of the system
  (put 'zero? '(scheme-number) zero?)
  (put 'zero? '(rational) zero-rational?)
  (put 'zero? '(complex) zero-complex?)
  'done)

(install-zero-package)

(define (=zero? x) (apply-generic 'zero? x))
