;; computes an approximation of a cube root of x
(define (cbrt x)
  (define (improve guess)
    (/ (+ (/ x (square guess))
	  (* 2 guess))
       3))
  (define (cbrt-iter guess next-guess)
    (define good-enough?
      (= guess next-guess))
    (if good-enough?
	guess
	(cbrt-iter next-guess
		   (improve next-guess))))
  (cbrt-iter 1.0 (improve 1.0)))
