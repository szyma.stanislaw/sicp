;; N -> [List-of [List N N]]
(define (unique-pairs n)
  (define (enumerate-interval low high)
    (if (> low high)
	'()
	(cons low (enumerate-interval (+ low 1) high))))
  (define (flatmap proc seq)
    (fold-right append '() (map proc seq)))
  (flatmap (lambda (i)
	     (map (lambda (j) (list i j))
		  (enumerate-interval 1 (- i 1))))
	   (enumerate-interval 1 n)))

(define (prime-sum-pairs n)
  (define (prime-sum? pair)
    (prime? (+ (car pair) (cadr pair))))
  (define (make-pair-sum pair)
    (list (car pair) (cadr pair) (+ (car pair) (cadr pair))))
  (map make-pair-sum
       (filter prime-sum?
	       (unique-pairs n))))
