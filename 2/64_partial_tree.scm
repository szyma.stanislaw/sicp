;; Node is a Number

(define (make-tree entry left right)
  (list entry left right))
;; Tree is one of:
;; - '()
;; - Number
;; - [List Node LTree RTree]
;; Constraint:
;; LTree < Node < RTree

;; selectors
(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))

;; [Ordered-list-of X] -> Tree
(define (list->tree elements)
  (car (partial-tree elements (length elements))))

;; [Ordered-list-of X] Number -> (cons Tree [List-of X])
(define (partial-tree elts n)
  (if (= n 0)
      (cons '() elts)
      (let ((left-size (quotient (- n 1) 2))) ;; (quotient a b) == (floor (/ a b))
	(let ((left-result (partial-tree elts left-size)))
	  (let ((left-tree (car left-result))
		(non-left-elts (cdr left-result))
		(right-size (- n (+ left-size 1))))
	    (let ((this-entry (car non-left-elts))
		  (right-result (partial-tree (cdr non-left-elts) ;; local vars in racket look much better
					      right-size)))
	      (let ((right-tree (car right-result))
		    (remaining-elts (cdr right-result)))
		(cons (make-tree this-entry left-tree right-tree)
		      remaining-elts))))))))

;; partial-tree first determines the number of elements in each subtree.
;; Since one element in the list is reserved for an entry, the left and
;; the right branch must have in total (- n 1) elements. Partial-tree
;; determines that the first (floor (/ (- n 1) 2)) elements belong to the
;; left one, the first from the remaining is the entry element, and the rest
;; belongs to the right subtree. Thus the result of applying partial-tree to
;; an ordered list is a tree whose left branch is the result of applying
;; partial-tree to the first (floor (/ (- n 1) 2)) elements, whose entry
;; element is the (+ 1 (floor (/ (- n 1) 2)))th one, and the right branch
;; is the result of applying the same procedure to the remaining list.

;; T = O(n), because with each call on a list with n elements, two
;; subsequent calls on sublists with app. n/2 elements are produced.
