;; Tree is one of:
;; - '()
;; - X 
;; - (cons Tree Tree)

;; Tree -> N
(define (count-leaves tree)
  (cond ((null? tree) 0)
	((not (pair? tree)) 1)
	(else (+ (count-leaves (car tree))
		 (count-leaves (cdr tree))))))

(define sample-tree (list 1 (list 2 (list 3 4))))
(newline)
(display (count-leaves sample-tree))

;; there should be some clever way to embed images into source code

