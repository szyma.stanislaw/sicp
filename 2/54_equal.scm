;; [List-of X] [List-of Y] -> Boolean
;; are the two lists equal
(define (equal? a b)
  (cond ((and (null? a) (null? b)) #t)
	((or (null? a) (null? b)) #f)
	((eq? (car a) (car b))
	 (equal? (cdr a) (cdr b)))
	(else #f)))
