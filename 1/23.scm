;; looking at primes and their elapsed times
;; necessary to pass the primality test

;; smallest primes greater than 10^10 
;; 10000000019 *** .08999999999999986 ;; result from ex. 1.22
;; 10000000019 *** .06                ;; result using improved smallest-divisor

;; 10000000033 *** .08999999999999986
;; 10000000033 *** .06

;; 10000000061 *** .08999999999999986
;; 10000000061 *** .06


;; smallest primes greater than 10^12
;; 1000000000039 *** .9100000000000001
;; 1000000000039 *** .5800000000000001

;; 1000000000061 *** .9199999999999999
;; 1000000000061 *** .5800000000000001

;; 1000000000063 *** .9199999999999999
;; 1000000000063 *** .5599999999999998


;; smallest primes greater than 10^14
;; 100000000000031 *** 9.14
;; 100000000000031 *** 5.549999999999999

;; 100000000000067 *** 9.120000000000001
;; 100000000000067 *** 5.549999999999999

;; 100000000000097 *** 9.29
;; 100000000000097 *** 5.57


;; smallest primes greater than 10^16
;; 10000000000000061 *** 91.24000000000001
;; 10000000000000061 *** 55.97

;; 10000000000000069 *** 91.69
;; 10000000000000069 *** 56.269999999999996

;; 10000000000000079 *** 91.21999999999997
;; 10000000000000079 *** 55.52000000000001


;; We see that the improved modification of the
;; smallest-divisor procedure does actually run
;; about 1.6 times faster than the version from
;; ex. 1.22 and not twice as fast. For that might
;; account a new function call with another conditional.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; computes a^b
;; I am looking for large primes
(define (exp a b)
  (define (iter a b product)
    (cond ((= b 0) product)
	  ((even? b)
	   (iter (square a) (/ b 2) product))
	  (else
	   (iter a (- b 1) (* product a)))))
  (iter a b 1))

;; if the procedure stumbles upon a prime it
;; in [a, b[ interval it embeds to it elapsed
;; time required to pass the primality test
(define (search-for-primes a b)
  (define (odd-iter lower)
    (cond ((>= lower b) b)
	  (else (timed-prime-test lower)
		(odd-iter (+ lower 2)))))
  (odd-iter (if (not (even? a))
		a
		(+ a 1))))

(define (even? n)
  (= (remainder n 2) 0))

;; if n is prime it prints n and shows after three asterisks
;; elapsed time needed to check it, otherwise only prints n
(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

;; N -> Boolean
;; is n prime
(define (prime? n)
  (= n (smallest-divisor n)))

(define (smallest-divisor n)
  (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
	((divides? test-divisor n) test-divisor)
	(else (find-divisor n (next test-divisor)))))

(define (next n)
  (if (= n 2)
      3
      (+ n 2)))

(define (divides? a b)
  (= (remainder b a) 0))
