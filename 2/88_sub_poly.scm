(load "87_zero_poly.scm")

(define (install-negation-package)
  ;; internal-procedures
  (define (negate-rational x)
    (define (numer x) (car x))
    (define (denom x) (cdr x))
    (make-rational (- (numer x)) (denom x)))
  (define (negate-complex z)
    (make-complex-from-real-image (negate (real-part z))
				  (negate (imag-part z))))
  (define (negate-poly p)
    (define (variable p) (car p))
    (define (term-list p) (cdr p))
    (define (order t) (car t))
    (define (coeff t) (cadr t))
    (define (make-term order coeff)
      (list order coeff))
    (make-polynomial (variable p)
		     (map (lambda (t) (make-term
				       (order t)
				       (negate (coeff t))))
			  (term-list p))))
    ;; interface for the rest of the system
  (put 'negate '(scheme-number) -)
  (put 'negate '(rational) negate-rational)
  (put 'negate '(complex) negate-complex)
  (put 'negate '(polynomial) negate-poly)
  'done)

(install-negation-package)

(define (negate x) (apply-generic 'negate x))


(define (install-sub-poly-package)
  ;; internal procedures
  (define (tag p) (attach-tag 'polynomial p)) ;; I know it's cheating
  (define (sub-poly p1 p2)
    (add (tag p1) (negate (tag p2))))
  ;; interface for the rest of the system
  (put 'sub '(polynomial polynomial) sub-poly)
  'done)

(install-sub-poly-package)



