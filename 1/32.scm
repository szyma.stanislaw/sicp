;; [X X -> Y] X [X -> X] X [X -> X] X
;; -> Y
(define (accumulate combiner null-value term a next b)
  (if (> a b)
      null-value
      (combiner (term a)
		(accumulate combiner null-value term (next a) next b))))

;; iterative version of accumulate
(define (accumulate-iter combiner null-value term a next b)
  (define (iter a result)
    (if (> a b)
	result
	(iter (next a) (combiner result (term a)))))
  (iter a null-value))


;; it should work
(define (sum term a next b)
  (accumulate + 0 term a next b))

(define (sum-iter term a next b)
  (accumulate-iter + 0 term a next b))


(define (product term a next b)
  (accumulate * 1 term a next b))

(define (product-iter term a next b)
  (accumulate-iter * 1 term a next b))

(newline)
(display (sum (lambda (x) x) 1 (lambda (x) (+ x 1)) 10)) ;; == 55
(newline)
(display (sum-iter (lambda (x) x) 1 (lambda (x) (+ x 1)) 10)) ;; == 55
(newline)
(display (product (lambda (x) x) 1 (lambda (x) (+ x 1)) 6)) ;; == 720
(newline)
(display (product-iter (lambda (x) x) 1 (lambda (x) (+ x 1)) 6)) ;; == 720
(newline)

